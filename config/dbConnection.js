
/**
 * dbConnection.js
 * @description :: database connection using sequelize
 */

const { Sequelize, } = require('sequelize');
const dbConfig = require('./db');

const sequelize = new Sequelize(`${dbConfig.dialect}://${dbConfig.USER}:${dbConfig.PASSWORD}@${dbConfig.HOST}:${dbConfig.port}/${dbConfig.DB}`, { alter: true });
module.exports = sequelize;
