/**
 * userController.js
 * @description :: exports user related methods
 */

const { user, userCredentials, property, file, company } = require('../../../model/index');
const dbService = require('../../../utils/dbService');
const _ = require('lodash');
const userSchemaKey = require('../../../utils/validation/userValidation');
const { ADMIN_ACCESS_TYPE } = require('../../../constants/propertyConstant')
const validation = require('../../../utils/validateRequest');
const walletService = require('../../../services/wallet');
const userConstant = require('../../../constants/userConstant');
const { Sequelize } = require('sequelize');

/**
 * @description : find all records of User from table based on query and options.
 * @param {Object} req : request including option and query. {query, options : {page, limit, includes}, isCountOnly}
 * @param {Object} res : response contains data found from table.
 * @return {Object} : found Property(s). {status, message, data}
 */
const findAllUser = async (req, res) => {
    /*
   #swagger.tags = ['User (Admin)'] 
   #swagger.security = [{
        "bearerAuth": []
  }] */
    let dataToFind = {};
    try {
        dataToFind = req.body;
        let { page, size } = dataToFind.options;
        let options = {}, query = {}, result = {}, paginator = {}, sortCondition = [], querySearch = {}, queryAttr = {};
        let foundUsers, sortbyBalance = false, balanceSortType;
        let validateRequest = validation.validateFilterWithJoi(
            dataToFind,
            userSchemaKey.findFilterKeys,
            user.tableAttributes
        );
        if (!validateRequest.isValid) {
            return res.validationError({ message: `${validateRequest.message}` });
        }
        if (dataToFind.options.sort !== undefined) {
            let sortArray = dataToFind.options.sort;
            for (let key in sortArray) {
                if (key != "maticBalance") {
                    sortCondition.push([`${key}`, sortArray[key]]);
                }
            }
        }
        if (dataToFind.options !== undefined) {
            options = dataToFind.options;
        }
        if (options.sort && options.sort.maticBalance) {
            sortbyBalance = true;
            balanceSortType = options.sort.maticBalance;
            delete options.sort.maticBalance;
        }
        if (dataToFind.query !== undefined) {
            queryAttr = dataToFind.query;
        }
        if (!_.isEmpty(dataToFind.search)) {
            let condition = [];
            userConstant.userAttribute.forEach(element => {
                condition.push({
                    [element]: {
                        '$like': `%${dataToFind.search}%`
                    }
                })
            });
            querySearch = {
                '$or': condition
            };
        }
        query = { ...querySearch, ...queryAttr }
        query = dbService.queryBuilderParser(query);
        foundUsers = await user.findAll({
            attributes: {
                include: [[Sequelize.fn("COUNT", Sequelize.col("properties.userId")), "propCount"]]
            },
            include: [{
                model: userCredentials,
                attributes: ['email']
            }, {
                model: property,
                attributes: []
            }],
            group: ['user.id', 'userCredential.id', 'properties.userId'],
            having: Sequelize.where(Sequelize.fn("COUNT", Sequelize.col('properties.userId')), '>=', 0),
            where: { ...query },
            order: sortCondition,
            subQuery: false,
        });
        if (!foundUsers.length) {
            return res.recordNotFound();
        }
        foundUsers = _.map(foundUsers, "dataValues");
        await Promise.all(foundUsers.map(async (userData) => {
            userData.RWONFTs = 0
            userData.maticBalance = 0
            if (userData.isCustodialWalletExist) {
                let balanceData = await walletService.getBalance(userData);
                userData.maticBalance = balanceData.data.maticBalance
            }
        }))
        if (sortbyBalance) {
            foundUsers.data = _.orderBy(foundUsers.data, ["maticBalance"], [balanceSortType]);
        }
        if (dataToFind.maticBalanceRange && dataToFind.maticBalanceRange.length) {
            let finalResult = [];
            dataToFind.maticBalanceRange.forEach((range) => {
                foundUsers.forEach((element) => {
                    if(element.maticBalance >= range[0]) {
                        if (element.maticBalance <= range[1] || range[1] == 0) {
                            finalResult.push(element);
                        }
                    }
                })
            })
            foundUsers = finalResult;
        }
        if (dataToFind.propertyRange && dataToFind.propertyRange.length) {
            let finalResult = [];
            dataToFind.propertyRange.forEach((range) => {
                foundUsers.forEach((element) => {
                    if(parseInt(element.propCount) >= range[0]) {
                        if (parseInt(element.propCount) <= range[1] || range[1] == 0) {
                            finalResult.push(element);
                        }
                    }
                })
            })
            foundUsers = finalResult;
        }
        if (!foundUsers.length) {
            return res.recordNotFound();
        }
        paginator.perPage = parseInt(size);
        paginator.pageCount = parseInt(Math.ceil(foundUsers.length / size));
        paginator.currentPage = parseInt(page);
        foundUsers = foundUsers.slice((page - 1) * size, ((page - 1) * size) + size);
        paginator.itemCount = parseInt(foundUsers.length);
        result.paginator = paginator;
        result.data = foundUsers;
        return res.success({ data: result });
    }
    catch (error) {
        console.error(error);
        return res.internalServerError({ data: error.message });
    }
};

/**
 * @description : find record of User from table by id;
 * @param {Object} req : request including id in request params.
 * @param {Object} res : response contains record retrieved from table.
 * @return {Object} : found User. {status, message, data}
 */
const getUser = async (req, res) => {
    /*
   #swagger.tags = ['User (Admin)'] 
   #swagger.security = [{
        "bearerAuth": []
  }] */
    try {
        let id = req.params.id;
        if (!id) {
            return res.badRequest({ message: 'Insufficient request parameters! id is required.' });
        }
        if (id == req.user.id) {
            return res.badRequest({ message: 'You can not search yourself.' });
        }
        let foundUser = await user.findOne({
            where: { id: id },
            include: [{
                model: userCredentials,
                required: true,
                attributes: ['email', 'userRole']
            }, {
                model: company,
                attributes: ['companyName']
            }, {
                model: property
            }],
        });
        if (!foundUser) {
            return res.recordNotFound();
        }
        if (ADMIN_ACCESS_TYPE.includes(foundUser.userCredential.userRole)) {
            return res.failure({ message: "You can not see Admin user details." });
        }
        foundUser.dataValues.maticBalance = 0
        if (foundUser.dataValues.isCustodialWalletExist) {
            let balanceData = await walletService.getBalance(foundUser.dataValues);
            foundUser.dataValues.maticBalance = balanceData.data.maticBalance
        }
        await Promise.all(_.map(foundUser.properties, async (val) => {
            let files;
            if (val.propertyProfileImages) {
                files = await file.findAll({
                    attributes: ['id', 'uri'],
                    where: {
                        id: val.propertyProfileImages
                    },
                });
                files = _.map(files, "dataValues");
                if (files.length) {
                    val.propertyProfileImages = files;
                }
            }
        }));
        foundUser.dataValues.totalProperty = foundUser.properties.length;
        return res.success({ data: foundUser });
    } catch (error) {
        console.error(error)
        return res.internalServerError();
    }
};

/**
 * @description : delete user for admin only
 * @param {Object} req : request for delete user
 * @param {Object} res : response for delete user
 * @return {Object} : response for delete user {status, message, data}
 */
const deleteUser = async (req, res) => {
    /*
   #swagger.tags = ['User (Admin)'] 
   #swagger.security = [{
        "bearerAuth": []
 }] */
    try {
        let params = req.params;
        if (params.id == req.user.id) {
            return res.badRequest({ message: 'You can not delete yourself.' });
        }
        let result = await user.findOne({
            where: { id: params.id },
            include: [{
                model: userCredentials,
                required: true,
                attributes: ['email', 'userRole']
            }],
        });
        if (!result) {
            return res.recordNotFound();
        }
        if (result.isDeleted) {
            return res.failure({ message: `User already deleted.` });
        }
        if (ADMIN_ACCESS_TYPE.includes(result.userCredential.userRole)) {
            return res.failure({ message: "You can not delete Admin user." });
        }
        let finalResult = await dbService.update(user, { id: params.id }, { isDeleted: true, isActive: false });
        if (!finalResult.length) {
            return res.failure({ message: "User not deleted." });
        }
        return res.success({ message: "User deleted successfully." });
    } catch (error) {
        console.error(error);
        return res.internalServerError({ message: error.message });
    }
};

const suspendUser = async (req, res) => {
    /*
   #swagger.tags = ['User (Admin)'] 
   #swagger.security = [{
        "bearerAuth": []
 }] */
    try {
        let params = req.params;
        if (params.id == req.user.id) {
            return res.badRequest({ message: 'You can not use toggle of suspend for yourself.' });
        }
        let result = await user.findOne({
            where: { id: params.id },
            include: [{
                model: userCredentials,
                required: true,
                attributes: ['email', 'userRole']
            }],
        });
        if (!result) {
            return res.recordNotFound();
        }
        if (result.isDeleted) {
            return res.failure({ message: `User already deleted.` });
        }
        if (ADMIN_ACCESS_TYPE.includes(result.userCredential.userRole)) {
            return res.failure({ message: "You can not use toggle of suspend for Admin user." });
        }
        let msg = "User account restored";
        if (result.isActive) {
            msg = "User account suspended";
        }
        let finalResult = await dbService.update(user, { id: params.id }, { isActive: !result.isActive });
        if (!finalResult.length) {
            return res.failure({ message: "User not suspended." });
        }
        return res.success({ message: msg });
    } catch (error) {
        console.error(error);
        return res.internalServerError({ message: error.message });
    }
};


module.exports = {
    findAllUser,
    getUser,
    deleteUser,
    suspendUser
};
