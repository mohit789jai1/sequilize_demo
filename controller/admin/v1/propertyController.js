/**
 * propertyController.js
 * @description :: exports property related methods
 */

const { property, userCredentials, user } = require('../../../model/index');
const Property = require('../../../model/property');
const dbService = require('../../../utils/dbService');
const propertySchemaKey = require('../../../utils/validation/propertyValidation');
const validation = require('../../../utils/validateRequest');
const _ = require('lodash');
const propertyConstant = require('../../../constants/propertyConstant');
const propertyService = require('../../../services/property');
const File = require('../../../model/file');

/**
 * @description : find all records of User Property from table based on query and options.
 * @param {Object} req : request including option and query. {query, options : {page, limit, includes}, isCountOnly}
 * @param {Object} res : response contains data found from table.
 * @return {Object} : found Property(s). {status, message, data}
 */
const findAllProperty = async (req, res) => {
  /*
 #swagger.tags = ['Property (Admin)'] 
 #swagger.security = [{
      "bearerAuth": []
}] */
  let dataToFind = {};
  try {
    dataToFind = req.body;
    let { page, size } = dataToFind.options;
    const { limit, offset } = await dbService.getPagination(page, size);
    let query = {}, result = {}, paginator = {}, sortCondition = [], querySearch = {}, queryAttr = {};
    let foundProperty;
    let validateRequest = validation.validateFilterWithJoi(
      dataToFind,
      propertySchemaKey.findFilterKeys,
      Property.tableAttributes
    );
    if (!validateRequest.isValid) {
      return res.validationError({ message: `${validateRequest.message}` });
    }
    if (dataToFind.options.sort !== undefined) {
      let sortArray = dataToFind.options.sort;
      for (let key in sortArray) {
          sortCondition.push([`${key}`, sortArray[key]]);
      }
    }
    if (dataToFind.query !== undefined) {
      queryAttr = dataToFind.query;
    }
    if (!_.isEmpty(dataToFind.search)) {
      let condition = [];
      propertyConstant.propertyAttribute.forEach(element => {
        condition.push({
          [element]: {
            '$like': `%${dataToFind.search}%`
          }
        })
      });
      querySearch = {
        '$or': condition
      };
    }
    query = { ...querySearch, ...queryAttr }
    query = dbService.queryBuilderParser(query);
    let count = await dbService.count(Property, query);
    if(!count) {
      return res.recordNotFound();
    }
    paginator.pageCount = parseInt(Math.ceil(count / limit));
    foundProperty = await Property.findAll({
      where: query,
      offset, limit,
      order: sortCondition,
      subQuery: false,
    });
    if (!foundProperty.length) {
      return res.recordNotFound();
    }
    foundProperty = _.map(foundProperty, "dataValues");
    await Promise.all(foundProperty.map(async (val) => {
      let files;
      if (val.propertyProfileImages) {
        files = await File.findAll({
          attributes: ['id', 'uri'],
          where: {
            id: val.propertyProfileImages
          },
        });
        files = _.map(files, "dataValues");
        if (files.length) {
          val.propertyProfileImages = files;
        }
      }
    }));
    foundProperty.forEach(element => {
      element.noOfNftCopies = 0;
      element.nftHoldersList = 0;
    });
    paginator.perPage = parseInt(size);
    paginator.currentPage = page;
    paginator.itemCount = parseInt(foundProperty.length);
    result.paginator = paginator;
    result.data = foundProperty;
    return res.success({ data: result });
  }
  catch (error) {
    return res.internalServerError({ data: error.message });
  }
};

/**
 * @description : find record of Property from table by id;
 * @param {Object} req : request including id in request params.
 * @param {Object} res : response contains record retrieved from table.
 * @return {Object} : found Property. {status, message, data}
 */
const getProperty = async (req, res) => {
  /*
 #swagger.tags = ['Property (Admin)'] 
 #swagger.security = [{
      "bearerAuth": []
}] */
  try {
    let id = req.params.id;
    let options = {
      include: [{
        model: userCredentials,
        attributes: ["email"]
      }, {
        model: user,
        attributes: ["isEmailVerified", "name", "mobileNo", "vaultId"]
      }],
    }
    let foundProperty = await dbService.findOne(Property, { id: id }, options);
    if (!foundProperty) {
      return res.recordNotFound();
    }
    await Promise.all(_.map(foundProperty, async (val) => {
      let files;
      if (val.propertyProfileImages) {
        files = await File.findAll({
          attributes: ['id', 'name', 'uri', 'size'],
          where: {
            id: val.propertyProfileImages
          },
        });
        files = _.map(files, "dataValues");
        if (files.length) {
          val.propertyProfileImages = files;
        }
      }
    }));
    foundProperty.dataValues.noOfNftCopies = 0;
    foundProperty.dataValues.nftHoldersList = 0;
    return res.success({ data: foundProperty });
  } catch (error) {
    return res.internalServerError();
  }
};

/**
 * @description : property registration by admin
 * @param {Object} req : request for register property
 * @param {Object} res : response for register property
 * @return {Object} : response for register property {status, message, data}
 */
const registerProperty = async (req, res) => {
  try {
    /*
      #swagger.tags = ['Property (Admin)'] 
      #swagger.security = [{
           "bearerAuth": []
    }] */
    let params = req.body;
    let userData = req.user;
    params = _.pick(params, [
      "id", "userId", "companyName", "contactName", "contactEmail", "contactPhone", "propertyOwnership",
      "addressLine1", "addressLine2", "country", "city", "postCode",
    ]);
    if (params.id) {
      if(params.userId){
        let isUserExist = await dbService.findOne(user, { id: params.userId, isEmailVerified: true, isKycVerified: true, isActive: true });
        if (!isUserExist) res.badRequest({ message: "You choosen invalid user!" });
      }
      const isPropertyExist = await dbService.findOne(property, { id: params.id });
      if (!isPropertyExist) {
        return res.validationError({ message: `Property not exists!.` });
      }
      let result = await dbService.update(property,
        { id: params.id, isFreeze: false },
        { ...params, propertyOwnership: propertyConstant.PROPERTY_OWNER_TYPES[params.propertyOwnership], updatedBy: userData.id });
      if (!result.length) {
        return res.failure({ message: "Your property update failed" });
      }
      return res.success({ message: "Your property updated successfully", data: result });
    } else {
      let result = await dbService.createOne(property, {
        ...params,
        propertyStatus: 0,
        addedBy: userData.id,
        updatedBy: userData.id,
      });
      return res.success({ message: "Your property register successfully", data: result });
    }
  } catch (error) {
    return res.internalServerError({ message: error.message });
  }
};


module.exports = {
  findAllProperty,
  getProperty,
  registerProperty
};
