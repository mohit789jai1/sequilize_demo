const fileService = require('../../../services/file');
const _ = require('lodash');
const slug = require('slug');
const sizeOf = require('image-size');


const createFile = async (req, res) => {
    /* #swagger.tags = ['File (Admin)'] */
    try {
        if (!req.files.length || !req.body.name) {
            return res.badRequest({ message: 'Insufficient request parameters! files and name is required.' });
        }
        const files = [];
        for (const file of req.files) {
            let fileData = await fileService.uploadFile(file, file.filename);
            let fileDir = file.destination + file.filename;
            const dimensions = sizeOf(fileDir);
            if (fileData && ['image/jpeg', 'application/pdf', 'image/png', 'video/mp4'].includes(file.mimetype)) {
                const fileObj = {
                    name: req.body.name || fileData.key,
                    uri: fileData.Location,
                    mimeType: file.mimetype,
                    size: file.size,
                    height: dimensions.height,
                    width: dimensions.width,
                    slug: slug(`${fileData.key}`),
                    type: req.body.type || null,
                    link: req.body.link || null,
                    title: req.body.title || null,
                    description: req.body.description || null,
                };
                files.push(fileObj);
            } else {
                return res.badRequest({ message: 'File type is not supported.' });
            }
        }
        const createdFiles = await fileService.createFile(files);
        let result = await fileService.find({ id: { $in: _.map(createdFiles, 'id') } });
        return res.success({ data: result });
    } catch (error) {
        console.error(error);
        return res.internalServerError({ message: error.message });
    }
};

const deleteFile = async (req, res) => {
    /* #swagger.tags = ['File (Admin)'] */
    try {
        if (!req.params.id) {
            return res.badRequest({ message: 'Insufficient request parameters! id is required.' });
        }
        let file = await fileService.getFileById(req.params.id);
        if (!file) {
            return res.recordNotFound();
        }
        if (file.isDeleted) {
            return res.failure({ message: "File already deleted" });
        }
        let result = await fileService.deleteFileById(req.params.id);
        if (!result) {
            return res.recordNotFound();
        }
        let delResult = await fileService.deleteFileRecord(req.params.id);
        if (delResult.flag) {
            return res.failure({ message: delResult.message });
        }
        return res.success({ message: delResult.message });
    }
    catch (error) {
        console.error(error);
        return res.internalServerError({ message: error.message });
    }
};

module.exports = {
    createFile,
    deleteFile
}