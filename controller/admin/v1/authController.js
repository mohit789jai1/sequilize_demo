/**
 * authController.js
 * @description :: exports authentication methods
 */
const authService = require('../../../services/auth');
const kycService = require('../../../services/kyc');
const tokenService = require('../../../services/token');
const authConstant = require('../../../constants/authConstant');
/**
 * @description : login with email and password
 * @param {Object} req : request for login 
 * @param {Object} res : response for login
 * @return {Object} : response for login {status, message, data}
 */
const login = async (req, res) => {
    /* #swagger.tags = ['Auth (Admin)'] */
    try {
        let { email, password } = req.body;
        if (!email || !password) {
            return res.badRequest({ message: 'Insufficient request parameters! email and password is required.' });
        }
        let result = await authService.loginUser(email, password, authConstant.PLATFORM.ADMIN);
        if (result.flag) {
            return res.failure({ message: result.message, data: null });
        }
        return res.success({
            data: result.data,
            message: 'Login successful.'
        });
    } catch (error) {
        console.error(error)
        return res.internalServerError({ message: error.message });
    }
};

/**
 * @description : refresh token
 * @param {Object} req : request for refreshToken
 * @param {Object} res : response for refreshToken
 * @return {Object} : response for refreshToken  {status, message, data}
 */
const refreshToken = async (req, res) => {
    /* #swagger.tags = ['Auth (Admin)'] */
    const params = req.body;
    try {
        if (!params.token) {
            return res.badRequest({ message: 'Insufficient request parameters! token is required.' });
        }
        let token = await tokenService.refreshToken(params, authConstant.JWT.ADMIN_TOKEN_SECRET);
        return res.success({ message: 'Token refreshed Successfully!', data: token });
    } catch (error) {
        console.error(error);
        return res.internalServerError({ message: error.message });
    }
};

/**
 * @description : kyc webhook
 * @param {Object} req : request for kyc webhook
 * @param {Object} res : response for kyc webhook
 * @return {Object} : response for kyc webhook  {status, message, data}
 */
const kycWebhook = async (req, res) => {
    const params = req.body;
    try {
        let isSignatureVerified = await kycService.verifySignature(req);
        if (!isSignatureVerified) {
            return res.failure({ message: isSignatureVerified.message });
        }
        let kycUpdate = await kycService.updateKycStatusByWebhook(params);
        if (kycUpdate.flag) {
            return res.failure({ message: kycUpdate.message, data: null });
        }
        return res.success({ message: 'KYC update Successfully!', data: kycUpdate });
    } catch (error) {
        console.error(error);
        return res.internalServerError({ message: error.message });
    }
};

module.exports = {
    login,
    refreshToken,
    kycWebhook
};
