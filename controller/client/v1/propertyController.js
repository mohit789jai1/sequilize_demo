/**
 * propertyController.js
 * @description :: exports property related methods
 */

const { user, userCredentials, property, file, propertyReaction } = require('../../../model/index');
const Property = require('../../../model/property');
const dbService = require('../../../utils/dbService');
const propertyConstant = require('../../../constants/propertyConstant');
const propertySchemaKey = require('../../../utils/validation/propertyValidation');
const validation = require('../../../utils/validateRequest');
const _ = require('lodash')
const propertyService = require('../../../services/property');
/**
 * @description : property registration 
 * @param {Object} req : request for register property
 * @param {Object} res : response for register property
 * @return {Object} : response for register property {status, message, data}
 */
const registerProperty = async (req, res) => {
  try {
    /* 
       #swagger.tags = ['Property (Client)']
       #swagger.security = [{
              "bearerAuth": []
       }] */
    let params = req.body;
    let userData = req.user;
    params = _.pick(params, [
      "id", "companyName", "contactName", "contactEmail", "contactPhone", "propertyOwnership",
      "addressLine1", "addressLine2", "country", "city", "postCode",
    ]);
    if (params.id) {
      const isPropertyExist = await dbService.findOne(property, { id: params.id });
      if (!isPropertyExist) {
        return res.validationError({ message: `Property not exists!.` });
      }
      let { userId } = isPropertyExist;
      if (userId != userData.id) res.badRequest({ message: 'You are not allowed to update this property' });
      let result = await dbService.update(property, { id: params.id, userId: userData.id, isFreeze: false }, { ...params, propertyOwnership: propertyConstant.PROPERTY_OWNER_TYPES[params.propertyOwnership] });
      if (!result.length) {
        return res.failure({ message: "Your property update failed" });
      }
      return res.success({ message: "Your property updated successfully", data: result });
    } else {
      let result = await dbService.createOne(property, {
        userId: userData.id,
        ...params,
        propertyStatus: 0
      });
      return res.success({ message: "Your property register successfully", data: result });
    }
  } catch (error) {
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : find all records of User Property from table based on query and options.
 * @param {Object} req : request including option and query. {query, options : {page, limit, includes}}
 * @param {Object} res : response contains data found from table.
 * @return {Object} : found Property(s). {status, message, data}
 */
const findAllProperty = async (req, res) => {
  /* 
         #swagger.tags = ['Property (Client)']
         #swagger.security = [{
                "bearerAuth": []
         }] */
  let dataToFind = {};
  try {
    dataToFind = req.body;
    let { page, size } = dataToFind.options;
    let query = {}, result = {}, paginator = {}, sortCondition = [], queryAttr = {}, querySearch = {}, range = {};
    let foundProperty, isFavouriteOnly = {}, isFavReq = false;
    let validateRequest = validation.validateFilterWithJoi(
      dataToFind,
      propertySchemaKey.findFilterKeys,
      Property.tableAttributes
    );
    if (!validateRequest.isValid) {
      return res.validationError({ message: `${validateRequest.message}` });
    }
    if (dataToFind.options.sort !== undefined) {
      let sortArray = dataToFind.options.sort;
      for (let key in sortArray) {
        sortCondition.push([`${key}`, sortArray[key]]);
      }
    }
    if (dataToFind.query !== undefined) {
      queryAttr = dataToFind.query;
    }
    if (!_.isEmpty(dataToFind.search)) {
      let condition = [];
      propertyConstant.propertyAttribute.forEach(element => {
        condition.push({
          [element]: {
            '$like': `%${dataToFind.search}%`
          }
        })
      });
      querySearch = {
        '$or': condition, 
      };
    }
    if (dataToFind.dateRange != undefined) {
      range = {
        'createdAt': {
          '$gte': `${dataToFind.dateRange.start}`,
          '$lte': `${dataToFind.dateRange.end}`,
        }
      }
    }
    if(dataToFind.isFavouriteOnly != undefined) {
      if(dataToFind.isFavouriteOnly) {
        isFavReq = true;
        isFavouriteOnly = {
          userId: req.user.id
        }
      }
    }
    query = { ...querySearch, ...queryAttr, ...range }
    query = dbService.queryBuilderParser(query);
    foundProperty = await Property.findAll({
      include: [{
        model: propertyReaction,
        attributes: ["id", "propertyId", "userId", "reactionType"],
        required: isFavReq,
        where: {
          ...isFavouriteOnly
        }
      }],
      where: query,
      order: sortCondition,
      subQuery: false,
    });
    if (!foundProperty.length) {
      return res.recordNotFound();
    }
    foundProperty = _.map(foundProperty, "dataValues");
    await Promise.all(_.map(foundProperty, async (val) => {
      let files;
      if (val.propertyProfileImages) {
        files = await file.findAll({
          attributes: ['id', 'uri'],
          where: {
            id: val.propertyProfileImages
          },
        });
        files = _.map(files, "dataValues");
        if (files.length) {
          val.propertyProfileImages = files;
        }
      }
    }));
    foundProperty.forEach(element => {
      element.noOfNftCopies = 0;
      element.nftHoldersList = 0;
      if(!element.propertyReactions.length) {
        element.isFavourite = false;
      } else {
        let flag = false;
        element.propertyReactions.forEach(value => {
          if(value.userId == req.user.id && value.reactionType == 1) {
            flag = true;
          }
        });
        element.isFavourite = flag;
      }
    });
    result.totalCount = foundProperty.length;
    paginator.perPage = parseInt(size);
    paginator.pageCount = parseInt(Math.ceil(foundProperty.length / size));
    paginator.currentPage = page;
    foundProperty = foundProperty.slice((page - 1) * size, ((page - 1) * size) + size);
    paginator.itemCount = parseInt(foundProperty.length);
    result.paginator = paginator;
    result.data = foundProperty;
    return res.success({ data: result });
  }
  catch (error) {
    return res.internalServerError({ data: error.message });
  }
};

/**
 * @description : find record of Property from table by id;
 * @param {Object} req : request including id in request params.
 * @param {Object} res : response contains record retrieved from table.
 * @return {Object} : found Property. {status, message, data}
 */
const getProperty = async (req, res) => {
  try {
    /* 
           #swagger.tags = ['Property (Client)']
           #swagger.security = [{
                  "bearerAuth": []
           }] */
    let id = req.params.id;
    let options = {
      include: [{
        model: userCredentials,
        attributes: ["email"]
      }, {
        model: user,
        attributes: ["isEmailVerified", "name", "mobileNo", "vaultId"]
      }],
    }
    let foundProperty = await dbService.findOne(Property, { id: id }, options);
    if (!foundProperty) {
      return res.recordNotFound();
    }
    await Promise.all(_.map(foundProperty, async (val) => {
      let files;
      if (val.propertyProfileImages) {
        files = await file.findAll({
          attributes: ['id', 'name', 'uri', 'size'],
          where: {
            id: val.propertyProfileImages
          },
        });
        files = _.map(files, "dataValues");
        if (files.length) {
          val.propertyProfileImages = files;
        }
      }
    }));
    foundProperty.dataValues.noOfNftCopies = 0;
    foundProperty.dataValues.nftHoldersList = 0;
    return res.success({ data: foundProperty });
  } catch (error) {
    return res.internalServerError();
  }
};


module.exports = {
  registerProperty,
  findAllProperty,
  getProperty
};
