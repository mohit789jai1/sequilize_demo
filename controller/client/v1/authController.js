/**
 * authController.js
 * @description :: exports authentication methods
 */
const authService = require('../../../services/auth');
const kycService = require('../../../services/kyc');
const tokenService = require('../../../services/token');
const { user, userAuthSettings, userCredentials, company } = require('../../../model/index');
const dbService = require('../../../utils/dbService');
const moment = require('moment');
const authConstant = require('../../../constants/authConstant');
const { checkUniqueFieldsInDatabase } = require('../../../utils/common');
const _ = require('lodash')
/**
 * @description : user registration 
 * @param {Object} req : request for register
 * @param {Object} res : response for register
 * @return {Object} : response for register {status, message, data}
 */
const register = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  try {
    let params = req.body;
    let companyId;
    if (!params.email || !params.password || !params.name) {
      return res.badRequest({ message: 'Insufficient request parameters! email and password is required.' });
    }
    params = _.pick(params, ["email", "password", "name", "companyName"])
    let checkUniqueFields = await authService.checkUserWithEmailVerifiedExist(params.email);
    if (checkUniqueFields.isDuplicate) {
      return res.validationError({ message: `${params.email} already exists.` });
    }
    if (!checkUniqueFields.new) {
      let userCredFound = await dbService.findOne(userCredentials, { email: params.email });
      const updateResult = await dbService.update(userCredentials, { userId: userCredFound.userId }, params);
      return res.success({ data: updateResult[0] });
    }
    if(params.companyName) {
      params.companyName = params.companyName.toLowerCase()
      let companyDetail = await dbService.findOne(company, { companyName: params.companyName });
      if(!companyDetail) {
        let create = await dbService.createOne(company, { companyName: params.companyName });
        companyId = create.dataValues.id;
      } else {
        companyId = companyDetail.id
      }
    }
    let result = await dbService.createOne(user, {
      ...params,
      companyId: companyId
    });
    const { id, isActive } = result;
    if (isActive) {
      const finalResult = await dbService.createOne(userCredentials, {
        userId: id,
        ...params,
        userRole: authConstant.USER_TYPES.User
      }); 
      return res.success({ data: finalResult });
    }
  } catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : send email or sms to user with OTP on forgot password
 * @param {Object} req : request for forgotPassword
 * @param {Object} res : response for forgotPassword
 * @return {Object} : response for forgotPassword {status, message, data}
 */
const forgotPassword = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  const params = req.body;
  try {
    if (!params.email) {
      return res.badRequest({ message: 'Insufficient request parameters! email is required' });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    let resultOfEmail = await authService.sendOTPByEmail(params.email, "FORGOT_PASSWORD");
    if (!resultOfEmail.flag) {
      return res.success({ message: 'OTP successfully send to your email.' });
    }
    return res.failure({ message: 'OTP can not be sent due to some issue try again later' });

  } catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : validate OTP
 * @param {Object} req : request for validateResetPasswordOtp
 * @param {Object} res : response for validateResetPasswordOtp
 * @return {Object} : response for validateResetPasswordOtp  {status, message, data}
 */
const validateResetPasswordOtp = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  const params = req.body;
  try {
    if (!params.code || !params.email) {
      return res.badRequest({ message: 'Insufficient request parameters.' });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    let response = await authService.verifyEmailOTP(params.email, params.code, "FORGOT_PASSWORD");
    if (!response.flag) {
      await authService.givePermissionForResetPassword(params.email, "FORGOT_PASSWORD_PERMISSION");
      return res.success({ message: response.message });
    }
    return res.failure({ message: response.message });
  } catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : reset password with code and new password
 * @param {Object} req : request for resetPassword
 * @param {Object} res : response for resetPassword
 * @return {Object} : response for resetPassword {status, message, data}
 */
const resetPassword = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  const params = req.body;
  try {
    if (!params.email || !params.newPassword) {
      return res.badRequest({ message: 'Insufficient request parameters! email and newPassword is required.' });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    let isPermissionToResetPassword = await authService.verifyResetPasswordPermission(params.email, "FORGOT_PASSWORD_PERMISSION");
    if (isPermissionToResetPassword.flag) {
      return res.failure({ message: isPermissionToResetPassword.message });
    }
    let response = await authService.resetPassword(params.email, params.newPassword);
    if (response.flag) {
      return res.failure({ message: response.data });
    }
    return res.success({ message: response.data });
  } catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : send OTP to user for login
 * @param {Object} req : request for sendOtpForLogin
 * @param {Object} res : response for sendOtpForLogin
 * @return {Object} : response for sendOtpForLogin {status, message, data}
 */
const sendOtpForLogin = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  try {
    let params = req.body;
    if (!params.email || (!params.password && (!params.isSocial && !params.socialLoginType))) {
      return res.badRequest({ message: 'Insufficient request parameters! email and password is required.' });
    }
    if (params.isSocial) {
      let otpResult = await authService.sendOTPForSocialLogin(params.email, {
        google: params.socialLoginType == authConstant.SOCIAL_LOGIN_TYPE.GOOGLE ? true : false,
        fb: params.socialLoginType == authConstant.SOCIAL_LOGIN_TYPE.FACEBOOK ? true : false
      });
      if (otpResult.flag) {
        return res.failure({ message: otpResult.message });
      }
      return res.success({ message: otpResult.message, data: otpResult.data });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `${params.email} user email not exists!.` });
    }
    let userData = await dbService.findOne(user, { id: isEmailExist.userId })
    if (!userData.isEmailVerified) {
      return res.failure({ message: "Email is not verified" });
    }
    let result = await authService.sendLoginOTP(params.email, params.password);
    if (result.flag) {
      return res.failure({ message: result.message });
    }
    return res.success({ message: result.message, data: result.data });
  } catch (error) {
    console.error(error)
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : login with email and OTP
 * @param {Object} req : request for loginWithOTP
 * @param {Object} res : response for loginWithOTP
 * @return {Object} : response for loginWithOTP {status, message, data}
 */
const loginWithOTP = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  const params = req.body;
  try {
    if (!params.code || !params.email || (!params.password && (!params.isSocial && !params.socialLoginType))) {
      return res.badRequest({ message: 'Insufficient request parameters! email, password and code is required.' });
    }
    if (params.isSocial) {
      let isVerifiedOTP = await authService.verifyEmailOTP(params.email, params.code, "LOGIN");
      if (isVerifiedOTP.flag) {
        return res.failure({ message: isVerifiedOTP.message });
      }
      let result = await authService.socialLoginWithOTP(params.email, {
        google: params.socialLoginType == authConstant.SOCIAL_LOGIN_TYPE.GOOGLE,
        fb: params.socialLoginType == authConstant.SOCIAL_LOGIN_TYPE.FACEBOOK
      });
      if (result.flag) {
        return res.failure({ message: result.message });
      }
      return res.success({ message: result.message, data: result.data });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `${params.email} user email not exists!.` });
    }
    let userData = await dbService.findOne(user, { id: isEmailExist.userId })
    if (!userData.isEmailVerified) {
      return res.failure({ message: "Email is not verified" });
    }
    let response = await authService.verifyEmailOTP(params.email, params.code, "LOGIN");
    if (response.flag) {
      return res.failure({ message: response.message });
    }
    let loginData = await authService.loginWithOTP(params.email, params.password, authConstant.PLATFORM.CLIENT);
    if (loginData.flag) {
      return res.badRequest({ message: loginData.message });
    }
    return res.success({
      data: loginData.data,
      message: 'Login successful.'
    });
  } catch (error) {
    console.error(error)
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : refresh token
 * @param {Object} req : request for refreshToken
 * @param {Object} res : response for refreshToken
 * @return {Object} : response for refreshToken  {status, message, data}
 */
const refreshToken = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  const params = req.body;
  try {
    if (!params.token) {
      return res.badRequest({ message: 'Insufficient request parameters! token is required.' });
    }
    let token = await tokenService.refreshToken(params);
    return res.success({ message: 'Token refreshed Successfully!', data: token });
  } catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};
/**
 * @description : Send Verification Code 
 * @param {Object} req : request for verification
 * @param {Object} res : response for verification
 * @return {Object} : response for verification {status, message, data}
 */
const sendEmailVerificationCode = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  try {
    let params = req.body;
    if (!params.email) {
      return res.badRequest({ message: 'Insufficient request parameters! email is required.' });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `${params.email} user email not exists!.` });
    }
    let userData = await dbService.findOne(user, { id: isEmailExist.userId })
    if (userData.isEmailVerified) {
      return res.failure({ message: "Email is already verified" });
    }
    if (params.altEmail && params.altEmail != params.email) {
      let checkUniqueFields = await checkUniqueFieldsInDatabase(userCredentials, ['email'], { email: params.altEmail }, 'REGISTER');
      if (checkUniqueFields.isDuplicate) {
        return res.validationError({ message: `Email already exists.` });
      }
      await dbService.update(userCredentials, { email: params.email }, { email: params.altEmail });
      params.email = params.altEmail;
    }
    let response = await authService.sendOTPByEmail(params.email, "REGISTER_VERIFY_EMAIL");
    if (response.flag) {
      return res.failure({ message: "Fail to send verification code!" });
    }
    return res.success({ message: "Your verification code send successfully" });
  } catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : Send Verification Code 
 * @param {Object} req : request for verification
 * @param {Object} res : response for verification
 * @return {Object} : response for verification {status, message, data}
 */
const verifyEmailOTP = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  try {
    let params = req.body;
    if (!params.email) {
      return res.badRequest({ message: 'Insufficient request parameters! email is required.' });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    let response = await authService.verifyEmailOTP(params.email, params.code, "REGISTER_VERIFY_EMAIL");
    if (!response.flag) {
      let userData = await dbService.findOne(userCredentials, { email: params.email });
      let userDetail = await dbService.findOne(user, {id: userData.userId});
      if(userDetail) {
        await dbService.update(company, { id: userDetail.companyId }, { isActive: true });
      }
      await dbService.update(user, { id: userData.userId }, { isEmailVerified: true });
      return res.success({ message: response.message });
    }
    return res.failure({ message: response.message });
  } catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : Send Verification Code 
 * @param {Object} req : request for verification
 * @param {Object} res : response for verification
 * @return {Object} : response for verification {status, message, data}
 */
const resendOTP = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  try {
    let params = req.body;
    let retryLimit, reactiveTime, type;
    if (!params.email) {
      return res.badRequest({ message: 'Insufficient request parameters! email is required.' });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    if (params.verifyEmail) {
      let userData = await dbService.findOne(user, { id: isEmailExist.userId })
      if (userData.isEmailVerified) {
        return res.failure({ message: "Email is already verified" });
      }
      retryLimit = "verifyEmailResendOTPRetryLimit";
      reactiveTime = "verifyEmailResendOTPReactiveTime";
      type = "REGISTER_VERIFY_EMAIL"
    } else if (params.login) {
      retryLimit = "loginResendOTPRetryLimit";
      reactiveTime = "loginResendOTPReactiveTime";
      type = "LOGIN"
    } else if (params.forgotPassword) {
      retryLimit = "resetPasswordResendOTPRetryLimit";
      reactiveTime = "resetPasswordResendOTPReactiveTime";
      type = "FORGOT_PASSWORD"
    } else {
      return res.badRequest({ message: 'Insufficient request parameters! type of resend otp is required.' });
    }
    let response = await authService.resendOTPByEmail({ userId: isEmailExist.userId, email: params.email, retryLimit, reactiveTime, type });
    let { flag, data, message } = response;
    if (!flag) {
      return res.success({ message, data });
    }
    return res.failure({ message, data });

  } catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : get kyc token 
 * @param {Object} req : request for get kyc token
 * @param {Object} res : response for get kyc token
 * @return {Object} : response for get kyc token {status, message, data}
 */
const getKycToken = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  try {
    let params = req.body;
    if (!params.email) {
      return res.badRequest({ message: 'Insufficient request parameters! email is required.' });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    let tokenData = await kycService.generateTokenForKyc({ email: params.email, userId: isEmailExist.userId });
    if (!tokenData.flag) {
      return res.success({ message: tokenData.message, data: tokenData.data });
    }
    return res.failure({ message: tokenData.message });
  } catch (error) {
    console.error(error)
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : get kyc token 
 * @param {Object} req : request for update kyc id
 * @param {Object} res : response for update kyc id
 * @return {Object} : response for update kyc id {status, message, data}
 */
const updateApplicantKycId = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  try {
    let params = req.body;
    if (!params.email && !params.applicantKycId) {
      return res.badRequest({ message: 'Insufficient request parameters! email and applicant kyc id is required.' });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    const isUserExist = await dbService.findOne(user, { id: isEmailExist.userId });
    if (!isUserExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    if (isUserExist.isKycVerified) {
      return res.validationError({ message: `Kyc Already verified!` });
    }
    let userData = await kycService.updateApplicantForKyc({ applicantKycId: params.applicantKycId, id: isEmailExist.userId }, 'update');
    if (!userData.flag) {
      return res.success({ message: userData.message, data: userData.data });
    }
    return res.failure({ message: userData.message, data: userData.data });
  } catch (error) {
    console.error(error)
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : get kyc token 
 * @param {Object} req : request for verify kyc applicant id
 * @param {Object} res : response for verify kyc id
 * @return {Object} : response for verify kyc {status, message, data}
 */
const verifyApplicantKycId = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  try {
    let params = req.body;
    if (!params.email) {
      return res.badRequest({ message: 'Insufficient request parameters! email is required.' });
    }
    const isEmailExist = await dbService.findOne(userCredentials, { email: params.email });
    if (!isEmailExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    const isUserExist = await dbService.findOne(user, { id: isEmailExist.userId });
    if (!isUserExist) {
      return res.validationError({ message: `User not exists with this email!.` });
    }
    if (isUserExist.isKycVerified) {
      return res.success({
        message: "kyc verified successfully",
        data: { isKycVerified: true, kycStatus: authConstant.KYC_STATUS.completed }
      });
    }
    let userData = await kycService.updateApplicantForKyc({ applicantKycId: isUserExist.applicantKycId, id: isUserExist.id }, 'verify');
    if (!userData.flag) {
      return res.success({ message: userData.message, data: userData.data });
    }
    return res.failure({ message: userData.message });
  } catch (error) {
    console.error(error)
    return res.internalServerError({ message: error.message });
  }
};

/**
 * @description : get company list
 * @param {Object} req : request for company list
 * @param {Object} res : response for companies
 * @return {Object} : response for company list {status, message, data}
 */
const getCompanyList = async (req, res) => {
  /* #swagger.tags = ['Auth (Client)'] */
  try {
    let companyDetail = await dbService.findAll(company, { isActive: true });
    if (!companyDetail.length) {
      return res.recordNotFound();
    }
    return res.success({ data: companyDetail });
  } catch (error) {
    console.error(error)
    return res.internalServerError({ message: error.message });
  }
};

module.exports = {
  register,
  sendOtpForLogin,
  loginWithOTP,
  forgotPassword,
  validateResetPasswordOtp,
  resetPassword,
  refreshToken,
  sendEmailVerificationCode,
  verifyEmailOTP,
  resendOTP,
  getKycToken,
  updateApplicantKycId,
  verifyApplicantKycId,
  getCompanyList
};
