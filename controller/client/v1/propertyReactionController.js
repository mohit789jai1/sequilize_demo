const { property, propertyReaction } = require('../../../model/index');
const dbService = require('../../../utils/dbService');
const propertyConstant = require('../../../constants/propertyConstant');
const _ = require('lodash')
const propertyService = require('../../../services/property');

const addPropertyReaction = async (req, res) => {
  try {
    /* 
        #swagger.tags = ['Property (Client)']
        #swagger.security = [{
               "bearerAuth": []
        }] */
    let params = req.body;
    let userData = req.user;
    if (!params.propertyId || !params.reactionType) {
      return res.badRequest({ message: 'Insufficient request parameters! Property Id and Reaction Type is required.' });
    }
    const isPropertyExist = await dbService.findOne(property, { id: params.propertyId });
    if (!isPropertyExist) {
      return res.validationError({ message: `Property not exists!.` });
    }
    const isPropertyReactionExist = await dbService.findOne(propertyReaction, { propertyId: params.propertyId, userId: userData.id, reactionType: propertyConstant.REACTION_TYPE[params.reactionType] });
    if (isPropertyReactionExist) {
      return res.validationError({ message: `You are already submit property reaction!.` });
    }
    let result = await dbService.createOne(propertyReaction, {
      userId: userData.id,
      propertyId: params.propertyId,
      reactionType: propertyConstant.REACTION_TYPE[params.reactionType]
    });
    return res.success({ data: result });
  } catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};

const deleteReaction = async (req, res) => {
  try {
    /* 
        #swagger.tags = ['Property (Client)']
        #swagger.security = [{
               "bearerAuth": []
        }] */
    let userData = req.user;
    if (!req.params.id) {
      return res.badRequest({ message: 'Insufficient request parameters! id is required.' });
    }
    let result = await dbService.findOne(propertyReaction, { propertyId: req.params.id });
    if (!result) {
      return res.recordNotFound();
    }
    if (result.userId != userData.id) {
      return res.badRequest({ message: 'You are not allowed to remove property reaction!' });
    }
    let finalResult = await dbService.destroy(propertyReaction, { propertyId: req.params.id });
    if (!finalResult) {
      return res.failure({ message: "Your reaction not remove" });
    }
    return res.success({ message: "Your reaction removed!" });
  }
  catch (error) {
    console.error(error);
    return res.internalServerError({ message: error.message });
  }
};


module.exports = {
  addPropertyReaction,
  deleteReaction,
}