const swaggerAutoGen = require('swagger-autogen')({ openapi: '3.0.0' })
const outputFile = './swagger/swagger_output.json'
const endpointsFiles = ['./routes/client/v1/index', './routes/admin/v1/index']

const doc = {
    openapi: '3.0.0', // YOU NEED THIS
    info: {
        title: 'demo',
        version: '1.0.0',
        description: 'API Documentation'
    },
    servers: [
        {
            url: "http://localhost:5000"
        }
    ],
    schemes: ['http', 'https'],
    basePath: '/',
    "tags": [
        {
            "name": "Auth (Client)"
        },
        {
            "name": "Property (Client)"
        },
        {
            "name": "Transaction (Client)"
        },
        {
            "name": "Auth (Admin)"
        },
        {
            "name": "User (Admin)"
        },
        {
            "name": "Property (Admin)"
        },
        {
            "name": "File (Client)"
        },
        {
            "name": "File (Admin)"
        }
    ],
    securityDefinitions: {
        bearerAuth: {
            type: 'http',
            scheme: 'bearer',
            bearerFormat: 'JWT'
        }
    },
    "outputDirectory": "swagger"
}
swaggerAutoGen(outputFile, endpointsFiles, doc).then(async () => {
    await import('./app.js');
});