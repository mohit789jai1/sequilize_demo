/**
 * userValidation.js
 * @description :: validate each post and put request as per user model
 */

const joi = require('joi');
const { options, isCountOnly, include, select } = require('./commonFilterValidation');
const { convertObjectToEnum } = require('../common');
const propertyConstant = require('../../constants/propertyConstant');

/** validation keys and properties of user */
exports.schemaKeys = joi.object({
    userId: joi.number().integer(),
    companyName: joi.string().allow(null).allow(''),
    contactName: joi.string().allow(null).allow(''),
    contactEmail: joi.string().allow(null).allow(''),
    contactPhone: joi.number().integer(),
    addressLine1: joi.string().allow(null).allow(''),
    addressLine2: joi.string().allow(null).allow(''),
    country: joi.string().allow(null).allow(''),
    city: joi.string().allow(null).allow(''),
    postCode: joi.number().integer(),
    propertyOwnership: joi.valid(...convertObjectToEnum(propertyConstant.PROPERTY_OWNER_TYPES)),
    propertyStatus: joi.number().integer(),
    isActive: joi.boolean(),
    isDeleted: joi.boolean()
}).unknown(true);

/** validation keys and properties of user for updation */
exports.updateSchemaKeys = joi.object({
    userId: joi.number().integer(),
    companyName: joi.string().allow(null).allow(''),
    contactName: joi.string().allow(null).allow(''),
    contactEmail: joi.string().allow(null).allow(''),
    contactPhone: joi.number().integer(),
    addressLine1: joi.string().allow(null).allow(''),
    addressLine2: joi.string().allow(null).allow(''),
    country: joi.string().allow(null).allow(''),
    city: joi.string().allow(null).allow(''),
    postCode: joi.number().integer(),
    propertyOwnership: joi.valid(...convertObjectToEnum(propertyConstant.PROPERTY_OWNER_TYPES)),
    propertyStatus: joi.number().integer(),
    isActive: joi.boolean(),
    isDeleted: joi.boolean(),
    id: joi.number().integer()
}).unknown(true);

let keys = ['query', 'where'];
/** validation keys and properties of user for filter documents from collection */
exports.findFilterKeys = joi.object({
    options: options,
    ...Object.fromEntries(
        keys.map(key => [key, joi.object({
            userId: joi.alternatives().try(joi.array().items(), joi.number().integer(), joi.object()),
            companyName: joi.alternatives().try(joi.array().items(), joi.string(), joi.object()),
            contactName: joi.alternatives().try(joi.array().items(), joi.string(), joi.object()),
            contactEmail: joi.alternatives().try(joi.array().items(), joi.string(), joi.object()),
            contactPhone: joi.alternatives().try(joi.array().items(), joi.string(), joi.object()),
            addressLine1: joi.alternatives().try(joi.array().items(), joi.string(), joi.object()),
            addressLine2: joi.alternatives().try(joi.array().items(), joi.string(), joi.object()),
            country: joi.alternatives().try(joi.array().items(), joi.string(), joi.object()),
            city: joi.alternatives().try(joi.array().items(), joi.string(), joi.object()),
            postCode: joi.alternatives().try(joi.array().items(), joi.number().integer(), joi.object()),
            propertyStatus: joi.alternatives().try(joi.array().items(), joi.number().integer(), joi.object()),
            isActive: joi.alternatives().try(joi.array().items(), joi.boolean(), joi.object()),
            isDeleted: joi.alternatives().try(joi.array().items(), joi.boolean(), joi.object()),
            id: joi.any()
        }).unknown(true),])
    ),
    isCountOnly: isCountOnly,
    include: joi.array().items(include),
    select: select

}).unknown(true);
