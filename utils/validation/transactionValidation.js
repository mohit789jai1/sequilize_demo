/**
 * userValidation.js
 * @description :: validate each post and put request as per user model
 */

 const joi = require('joi');
 const { options, isCountOnly, include, select } = require('./commonFilterValidation');

 let keys = ['query', 'where'];
 /** validation keys and properties of user for filter documents from collection */
 exports.findFilterKeys = joi.object({
     options: options,
     ...Object.fromEntries(
         keys.map(key => [key, joi.object({
             type: joi.alternatives().try(joi.array().items(), joi.string(), joi.object()),
             id: joi.any()
         }).unknown(true),])
     ),
     isCountOnly: isCountOnly,
     include: joi.array().items(include),
     select: select
 
 }).unknown(true);
 