/**
 * userValidation.js
 * @description :: validate each post and put request as per user model
 */

const joi = require('joi');
const { options, isCountOnly, include, select } = require('./commonFilterValidation');
const authConstant = require('../../constants/authConstant');
const { convertObjectToEnum } = require('../common');

let keys = ['query', 'where'];
/** validation keys and properties of user for filter documents from collection */
exports.findFilterKeys = joi.object({
    options: options,
    ...Object.fromEntries(
        keys.map(key => [key, joi.object({
            id: joi.any(),
            isActive: joi.alternatives().try(joi.boolean()),
            isDeleted: joi.alternatives().try(joi.boolean()),
            applicantKycId: joi.alternatives().try(joi.string()),
            isKycVerified: joi.alternatives().try(joi.boolean()),
            isEmailVerified: joi.alternatives().try(joi.boolean()),
            name: joi.alternatives().try(joi.string()),
            kycStatus: joi.alternatives().try(joi.array().items(), joi.valid(...convertObjectToEnum(authConstant.KYC_STATUS))),
            custodialWalletAddress: joi.alternatives().try(joi.string())
        }).unknown(false),])
    ),
    isCountOnly: isCountOnly,
    include: joi.array().items(include),
    select: select

}).unknown(true);
