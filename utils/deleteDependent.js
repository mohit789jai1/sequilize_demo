/**
 * deleteDependent.js
 * @description :: exports deleteDependent service for project.
 */

let User = require('../model/user');
let UserAuthSettings = require('../model/userAuthSettings');
let dbService = require('.//dbService');

const deleteUser = async (filter) => {
  try {
    let user = await dbService.findAll(User, filter);
    if (user && user.length) {
      user = user.map((obj) => obj.id);

      const userFilter = { $or: [{ addedBy: { $in: user } }, { updatedBy: { $in: user } }] };
      const userCnt = await dbService.destroy(User, userFilter);

      const userAuthSettingsFilter = { $or: [{ userId: { $in: user } }, { addedBy: { $in: user } }, { updatedBy: { $in: user } }] };
      const userAuthSettingsCnt = await dbService.destroy(UserAuthSettings, userAuthSettingsFilter);

      let deleted = await dbService.destroy(User, filter);
      return {
        user: userCnt.length + deleted.length,
        userAuthSettings: userAuthSettingsCnt.length
      };
    } else {
      return { user: 0 };
    }

  } catch (error) {
    throw new Error(error.message);
  }
};

const deleteUserAuthSettings = async (filter) => {
  try {
    return await dbService.destroy(UserAuthSettings, filter);
  } catch (error) {
    throw new Error(error.message);
  }
};

const countUser = async (filter) => {
  try {
    let user = await dbService.findAll(User, filter);
    if (user && user.length) {
      user = user.map((obj) => obj.id);

      const userFilter = { $or: [{ addedBy: { $in: user } }, { updatedBy: { $in: user } }] };
      const userCnt = await dbService.count(User, userFilter);

      const userAuthSettingsFilter = { $or: [{ userId: { $in: user } }, { addedBy: { $in: user } }, { updatedBy: { $in: user } }] };
      const userAuthSettingsCnt = await dbService.count(UserAuthSettings, userAuthSettingsFilter);

      return {
        user: userCnt,
        userAuthSettings: userAuthSettingsCnt
      };
    } else {
      return { user: 0 };
    }
  } catch (error) {
    throw new Error(error.message);
  }
};

const countUserAuthSettings = async (filter) => {
  try {
    const userAuthSettingsCnt = await dbService.count(UserAuthSettings, filter);
    return { userAuthSettings: userAuthSettingsCnt };
  } catch (error) {
    throw new Error(error.message);
  }
};

const softDeleteUser = async (filter, updateBody) => {
  try {
    let user = await dbService.findAll(User, filter, { id: 1 });
    if (user.length) {
      user = user.map((obj) => obj.id);

      const userFilter = { '$or': [{ addedBy: { '$in': user } }, { updatedBy: { '$in': user } }] };
      const userCnt = await dbService.update(User, userFilter, updateBody);

      const userAuthSettingsFilter = { '$or': [{ userId: { '$in': user } }, { addedBy: { '$in': user } }, { updatedBy: { '$in': user } }] };
      const userAuthSettingsCnt = await dbService.update(UserAuthSettings, userAuthSettingsFilter, updateBody);

      let updated = await dbService.update(User, filter, updateBody);

      return {
        user: userCnt.length + updated.length,
        userAuthSettings: userAuthSettingsCnt.length
      };
    } else {
      return { user: 0 };
    }
  } catch (error) {
    throw new Error(error.message);
  }
};

const softDeleteUserAuthSettings = async (filter, updateBody) => {
  try {
    const userAuthSettingsCnt = await dbService.update(UserAuthSettings, filter);
    return { userAuthSettings: userAuthSettingsCnt };
  } catch (error) {
    throw new Error(error.message);
  }
};

module.exports = {
  deleteUser,
  deleteUserAuthSettings,
  countUser,
  countUserAuthSettings,
  softDeleteUser,
  softDeleteUserAuthSettings
};
