/**
 * auth.js
 * @description :: express routes of authentication APIs
 */

const express = require('express');
const router = express.Router();
const authController = require('../../../controller/client/v1/authController');

router.route('/register').post(authController.register);
router.route('/send_login_otp').post(authController.sendOtpForLogin);
router.route('/login_with_otp').post(authController.loginWithOTP);
router.route('/forgot_password').post(authController.forgotPassword);
router.route('/validate_otp').post(authController.validateResetPasswordOtp);
router.route('/refresh_token').post(authController.refreshToken);
router.route('/reset_password').post(authController.resetPassword);
router.route('/send_otp').post(authController.sendEmailVerificationCode);
router.route('/verify_otp').post(authController.verifyEmailOTP);
router.route('/resend_otp').post(authController.resendOTP);
router.route('/generate_kyc_token').post(authController.getKycToken);
router.route('/update_kyc_id').post(authController.updateApplicantKycId);
router.route('/verify_kyc').post(authController.verifyApplicantKycId);
router.route('/get_company_list').get(authController.getCompanyList);

module.exports = router;
