/**
 * index route file of client platform.
 * @description: exports all routes of client platform.
 */
const express = require('express');
const router = express.Router();
router.use('/client/auth', require('./auth'));
router.use('/client/file',  require('./file'));

module.exports = router;
