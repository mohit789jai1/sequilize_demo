/**
 * auth.js
 * @description :: express routes of authentication APIs
 */

const express = require('express');
const router = express.Router();
const propertyController = require('../../../controller/client/v1/propertyController');
const reactionController = require('../../../controller/client/v1/propertyReactionController');
const auth = require('../../../middleware/auth');
const { PLATFORM } = require('../../../constants/authConstant')

router.route('/register_property').post(auth(PLATFORM.CLIENT), propertyController.registerProperty);
router.route('/get_property').post(auth(PLATFORM.CLIENT), propertyController.findAllProperty);
router.route('/get_property/:id').get(auth(PLATFORM.CLIENT), propertyController.getProperty);
router.route('/add_reaction').post(auth(PLATFORM.CLIENT), reactionController.addPropertyReaction);
router.route('/remove_reaction/:id').delete(auth(PLATFORM.CLIENT), reactionController.deleteReaction);

module.exports = router;
