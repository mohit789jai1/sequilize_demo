const express = require('express');
const router = express.Router();
const fileController = require('../../../controller/client/v1/fileController');
const auth = require('../../../middleware/auth');
const { PLATFORM } = require('../../../constants/authConstant')

router.route('/create').post(auth(PLATFORM.CLIENT), fileController.createFile);
router.route('/delete/:id').delete(auth(PLATFORM.CLIENT), fileController.deleteFile);

module.exports = router;
