/**
 * auth.js
 * @description :: express routes of authentication APIs
 */

const express = require('express');
const router = express.Router();
const userController = require('../../../controller/admin/v1/userController');
const auth = require('../../../middleware/auth');
const { PLATFORM } = require('../../../constants/authConstant')

router.route('/get_users').post(auth(PLATFORM.ADMIN), userController.findAllUser);
router.route('/get_user/:id').get(auth(PLATFORM.ADMIN), userController.getUser);
router.route('/delete_user/:id').delete(auth(PLATFORM.ADMIN), userController.deleteUser);
router.route('/suspend_user_toggle/:id').get(auth(PLATFORM.ADMIN), userController.suspendUser);

module.exports = router;
