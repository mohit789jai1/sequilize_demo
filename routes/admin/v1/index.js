/**
 * index route file of admin platform.
 * @description: exports all routes of admin platform.
 */
const express = require('express');
const router = express.Router();
router.use('/admin/auth', require('./auth'));
router.use('/admin/user', require('./user'));
router.use('/admin/file', require('./file'));

module.exports = router;
