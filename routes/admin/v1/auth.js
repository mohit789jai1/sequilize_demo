/**
 * auth.js
 * @description :: express routes of authentication APIs
 */

const express = require('express');
const router = express.Router();
const authController = require('../../../controller/admin/v1/authController');

router.post('/login', authController.login);
router.route('/refresh_token').post(authController.refreshToken);
router.route('/kyc_webhook').post(authController.kycWebhook);

module.exports = router;
