/**
 * property.js
 * @description :: express routes of property APIs
 */

const express = require('express');
const router = express.Router();
const propertyController = require('../../../controller/admin/v1/propertyController');
const auth = require('../../../middleware/auth');
const { PLATFORM } = require('../../../constants/authConstant')

router.route('/get_property').post(auth(PLATFORM.ADMIN), propertyController.findAllProperty);
router.route('/get_property/:id').get(auth(PLATFORM.ADMIN), propertyController.getProperty);
router.route('/register_property').post(auth(PLATFORM.ADMIN), propertyController.registerProperty);

module.exports = router;
