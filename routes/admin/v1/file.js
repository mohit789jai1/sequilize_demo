const express = require('express');
const router = express.Router();
const fileController = require('../../../controller/admin/v1/fileController');
const auth = require('../../../middleware/auth');
const { PLATFORM } = require('../../../constants/authConstant')

router.route('/create').post(auth(PLATFORM.ADMIN), fileController.createFile);
router.route('/delete/:id').delete(auth(PLATFORM.ADMIN), fileController.deleteFile);

module.exports = router;
