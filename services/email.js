const nodemailer = require('nodemailer');
const ejs = require('ejs');

module.exports = {
  sendMail: async (obj) => {
    let transporter = nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: 465,
      secure: true,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD
      }
    });
    if (!Array.isArray(obj.to)) {
      obj.to = [obj.to];
    }
    const htmlText = await ejs.renderFile(`${__basedir}${obj.template}/html.ejs`, obj.data);

    await Promise.all(obj.to.map((emailId) => {
      var mailOpts = {
        from: obj.from || 'ajay.t+test@techalchemy.co',
        to: emailId,
        subject: obj.subject,
        html: htmlText
      };
      transporter.sendMail(mailOpts, function (err, response) {
        if (err) {
          console.error(err.message);
        }
      });
    }));

    return true;
  }
};
