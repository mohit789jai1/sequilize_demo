const projectSetting = require('../model/projectSetting');
const axios = require('axios');
const dbService = require('../utils/dbService');
const { PROJECT_SETTING } = require('../constants/userConstant');

module.exports = {
    updateLiveTokenValue: async () => {
        let updateData = await axios.get('https://api.coingecko.com/api/v3/simple/price?ids=matic-network&vs_currencies=usd%2Ceur%2Cgbp');
        let isValueExist = await dbService.findOne(projectSetting, { type: PROJECT_SETTING.CURRENCY_TOKEN_VALUE });
        if (isValueExist) {
            await dbService.update(projectSetting, { type: PROJECT_SETTING.CURRENCY_TOKEN_VALUE }, { setting: updateData.data })
        } else {
            await dbService.createOne(projectSetting, { type: PROJECT_SETTING.CURRENCY_TOKEN_VALUE, setting: updateData.data })
        }

    },
};