const jwt = require('jsonwebtoken');
const { JWT } = require('../constants/authConstant');

/**
 * @description : service to generate JWT token for authentication.
 * @param {obj} user : user who wants to login.
 * @param {string} secret : secret for JWT.
 * @return {string}  : returns JWT token.
 */
const generateToken = (userId, secret) => {
    return {
        accessToken: signToken(userId, secret, JWT.ACCESS_TOKEN_EXPIRES_IN),
        refreshToken: signToken(userId, secret, JWT.REFRESH_TOKEN_EXPIRES_IN),
    };
};

const signToken = (userId, secret = JWT.CLIENT_TOKEN_SECRET, expiresIn = JWT.ACCESS_TOKEN_EXPIRES_IN) => {
    const options = {
        expiresIn: expiresIn,
        audience: userId.toString(),
    };
    return jwt.sign({}, secret, options);
};

const refreshToken = async (body, secret = JWT.CLIENT_TOKEN_SECRET) => {
    const result = await verifyJwtToken(body.token, secret);
    return {
        accessToken: signToken(result.aud, secret, JWT.ACCESS_TOKEN_EXPIRES_IN),
        refreshToken: signToken(result.aud, secret, JWT.REFRESH_TOKEN_EXPIRES_IN),
    };
}

const verifyJwtToken = async (refreshTokenData, secretKey) => {
    let result;
    try {
        result = jwt.verify(refreshTokenData, secretKey);
    } catch (err) {
        console.error(err.message)
        throw err;
    }
    return result;
};
module.exports = {
    generateToken,
    refreshToken
};