
const File = require('../model/file');
const fs = require('fs');
const mime = require('mime-types');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({ region: 'eu-west-2' });
const dbService = require('../utils/dbService');
/**
 * Create a file
 * @param {Object} fileBody
 * @returns {Promise<File>}
 */
const createFile = async (fileBody) => {
	return dbService.createOne(File, ...fileBody);
};

/**
 * find a file
 * @param {Object} fileBody
 * @returns {Promise<File>}
 */
const find = async (filter) => {
	return dbService.findOne(File, filter);
};

/**
 * upload File
 * @param {file} file
 * @returns {Promise<File>}
 */
const uploadFile = async (file, id) => {
	const ext = mime.extension(file.mimetype);
	const fileContent = fs.readFileSync(file.path);
	const params = {
		Bucket: process.env.BUCKET_NAME,
		Key: `${id}.${ext}`, // File name you want to save as in S3
		Body: fileContent,
		ContentType: file.mimetype,
		ACL: 'public-read'
	};
	return s3.upload(params).promise();
};
/**
 * Get file by id
 * @param {ObjectId} id
 * @returns {Promise<File>}
 */
const getFileById = async (id) => {
	return dbService.findOne(File, { id: id });
};

/**
 * Delete file by id
 * @param {ObjectId} fileId
 * @returns {Promise<File>}
 */
const deleteFileById = async (fileId) => {
	const file = await getFileById(fileId);
	if (!file) {
		throw new Error('File not found');
	}
	if (file.uri) {
		let key = file.uri.split('/');
		key = key.slice(-1)[0];
		let path = key.split('.');
		path = "uploads/" + path[0];
		let params = {
			Bucket: process.env.BUCKET_NAME,
			Key: key
		};
		await s3.deleteObject(params).promise();
		await fs.unlinkSync(path);
	}
	return file;
};

/**
 * Delete file by id in file database
 * @param {ObjectId} fileId
 *  @return {obj}  : returns file deletion response {flag,status}.
 */
const deleteFileRecord = async (fileId) => {
	let finalResult = await dbService.update(File, { id: fileId }, { isActive: false, isDeleted: true });
	if (!finalResult.length) {
		return {
			flag: true,
			message: "File deletion failed!",
			data: null
		};
	}
	return {
		flag: false,
		message: "File deleted successfully",
		data: finalResult.data
	};
}

module.exports = {
	find,
	createFile,
	uploadFile,
	getFileById,
	deleteFileById,
	deleteFileRecord
};
