/**
 * auth.js
 * @description :: service functions used in authentication
 */

const model = require('../model/index');
const axios = require('axios');
const dbService = require('../utils/dbService');
const { JWT, LOGIN_ACCESS, PLATFORM, MAX_LOGIN_RETRY_LIMIT, LOGIN_REACTIVE_TIME, FORGOT_PASSWORD_LIMITS, REDIS_EXP_TIME, MAX_RESEND_OTP_LIMIT, RESEND_OTP_REACTIVE_TIME, MAX_OTP_VALIDATE_RETRY_LIMIT, SOCIAL_LOGIN_TYPE, USER_TYPES, KYC_STATUS } = require('../constants/authConstant');
const common = require('../utils/common');
const moment = require('moment');
const bcrypt = require('bcrypt');
const emailService = require('./email');
const tokenService = require('./token');
const { client } = require('../middleware/redisClient');
const _ = require('lodash');
const { updateApplicantForKyc } = require('./kyc');
const { updateFirstTransactionStatus } = require('./transaction/baseTransaction');

const checkUserWithEmailVerifiedExist = async function (email) {
  let userCredFound = await dbService.findOne(model.userCredentials, { email });
  if (!userCredFound) {
    return { isDuplicate: false, new: true }
  }
  let userFound = await dbService.findOne(model.user, { id: userCredFound.userId });
  if (userFound && !userFound.isEmailVerified) {
    return { isDuplicate: false, new: false }
  }
  return { isDuplicate: true };
}

/**
 * @description : service of login user.
 * @param {string} username : username of user.
 * @param {string} password : password of user.
 * @param {string} platform : platform.
 * @return {obj} : returns authentication status. {flag, data}
 */
const loginUser = async (email, password, platform, flag = {}) => {
  try {
    let where = { email: email };
    const userCredentials = await dbService.findOne(model.userCredentials, where);
    if (!userCredentials) {
      return {
        flag: true,
        message: 'User not exists',
        data: null
      };
    }
    const user = await dbService.findOne(model.user, { id: userCredentials.userId });
    if (!user) {
      return {
        flag: true,
        message: 'User not exists',
        data: null
      };
    }
    let userAuth = await dbService.findOne(model.userAuthSettings, { userId: user.id });
    if (userAuth && userAuth.loginRetryLimit >= MAX_LOGIN_RETRY_LIMIT) {
      let userLimitExceed = await checkLimitOfLoginReactiveTime(userAuth, user);
      if (userLimitExceed.flag) {
        return userLimitExceed;
      }
    }
    if (password) {
      let isPasswordMatched = await userCredentials.isPasswordMatch(password);
      if (!isPasswordMatched) {
        await dbService.update(model.userAuthSettings, { userId: user.id }, { loginRetryLimit: userAuth.loginRetryLimit + 1 });
        return {
          flag: true,
          data: null,
          message: 'Incorrect Password'
        };
      }
    }
    if (flag.sendOTP) {
      return await sendOTPByEmail(email, "LOGIN");
    }
    const userData = user.toJSON();
    let token;
    if (!userCredentials.userRole) {
      return {
        flag: true,
        message: 'You have not been assigned any role',
        data: null
      };
    }
    if (platform == PLATFORM.CLIENT) {
      if (!LOGIN_ACCESS[userCredentials.userRole].includes(PLATFORM.CLIENT)) {
        return {
          flag: true,
          message: 'you are not allowed to access this platform',
          data: null
        };
      }
      token = tokenService.generateToken(userData.id, JWT.CLIENT_TOKEN_SECRET);
    }
    if (platform == PLATFORM.ADMIN) {
      if (!LOGIN_ACCESS[userCredentials.userRole].includes(PLATFORM.ADMIN)) {
        return {
          flag: true,
          message: 'you are not allowed to access this platform',
          data: null
        };
      }
      token = tokenService.generateToken(userData.id, JWT.ADMIN_TOKEN_SECRET);
    }
    let kyc = {}, transData = {};
    if (!user.isKycVerified) {
      kyc = await updateApplicantForKyc(user, 'verify');
    }
    if (platform == PLATFORM.CLIENT && user.isCustodialWalletExist && !user.isFirstMaticTransferHappen) {
      transData = await updateFirstTransactionStatus(user.id)
      if (!transData.isFirstMaticTransferHappen) {
        let lastTransaction = await dbService.findAll(model.transaction, { userId: user.id, type: 'DEPOSIT', }, { select: ['status'], sort: { id: -1 } })
        if (lastTransaction.length) {
          transData.lastTransactionStatus = lastTransaction[0].status
        } else {
          transData.lastTransactionStatus = null;
        }
      }
    }
    if (userAuth && userAuth.loginRetryLimit) {
      await dbService.update(model.userAuthSettings, { userId: user.id }, {
        loginRetryLimit: 0,
        loginReactiveTime: null
      });
    }
    let userToReturn = {
      ...userData,
      ...{ email },
      ...kyc.data,
      ...transData,
      token
    };
    return {
      flag: false,
      data: userToReturn
    };

  } catch (error) {
    console.error(error)
    throw new Error(error.message);
  }
};

/**
 * @description : service method used to check on login reactive time.
 */
const checkLimitOfLoginReactiveTime = async (userAuth, user) => {
  try {
    let now = moment();
    if (userAuth.loginReactiveTime) {
      let limitTime = moment(userAuth.loginReactiveTime);
      if (limitTime > now) {
        let expireTime = moment().add(LOGIN_REACTIVE_TIME, 'minute');
        if (limitTime <= expireTime) {
          return {
            flag: true,
            data: null,
            message: `you have exceed the number of limit.you can login after ${common.getDifferenceOfTwoDatesInTime(now, limitTime)}.`
          };
        }
        await dbService.update(model.userAuthSettings, { userId: user.id }, {
          loginReactiveTime: expireTime.toISOString(),
          loginRetryLimit: userAuth.loginRetryLimit + 1
        });
        return {
          flag: true,
          data: null,
          message: `you have exceed the number of limit.you can login after ${common.getDifferenceOfTwoDatesInTime(now, expireTime)}.`
        };
      } else {
        await dbService.update(model.userAuthSettings, { userId: user.id }, {
          loginReactiveTime: null,
          loginRetryLimit: 0
        });
        return {
          flag: false
        }
      }
    } else {
      // send error
      let expireTime = moment().add(LOGIN_REACTIVE_TIME, 'minute');
      await dbService.update(model.userAuthSettings, { userId: user.id }, {
        loginReactiveTime: expireTime.toISOString(),
        loginRetryLimit: userAuth.loginRetryLimit + 1
      });
      return {
        flag: true,
        data: null,
        message: `you have exceed the number of limit.you can login after ${common.getDifferenceOfTwoDatesInTime(now, expireTime)}.`
      };
    }
  } catch (error) {
    console.error(error)
    throw new Error(error.message);
  }
};
/**
 * @description : service method to send OTP for login.
 * @param {string} username : username of user.
 * @return {obj}  : returns status whether OTP is sent or not. {flag, data}
 */
const sendLoginOTP = async (email, password) => {
  try {
    return await loginUser(email, password, PLATFORM.CLIENT, { sendOTP: true });
  } catch (error) {
    throw new Error(error.message);
  }
};

/**
 * @description : service to login with OTP.
 * @param {string} username : username of user.
 * @param {string} password : password of user.
 * @param {string} platform : platform.
 * @return {obj}  : returns authentication object {flag,status}.
 */
const loginWithOTP = async (username, password, platform) => {
  try {
    return await loginUser(username, password, platform);
  } catch (error) {
    throw new Error(error.message);
  }
};

/**
 * @description : service to reset password.
 * @param {obj} userId : user`s id 
 * @param {string} newPassword : new password to be set.
 * @return {}  : returns status whether new password is set or not. {flag, data}
 */
const resetPassword = async (email, newPassword) => {
  try {
    let where = { email };
    newPassword = await bcrypt.hash(newPassword, 8);
    let updatedUser = await dbService.update(model.userCredentials, where, { password: newPassword });
    if (!updatedUser) {
      return {
        flag: true,
        data: 'Password is not reset successfully',
      };
    }
    await dbService.update(model.userAuthSettings, { userId: updatedUser[0].userId }, { loginRetryLimit: 0 });
    let mailObj = {
      subject: 'Reset Password',
      to: email,
      template: '/views/email/successfulPasswordReset',
      data: {
        isWidth: true,
        email: email || '-',
        message: 'Your password has been changed Successfully.'
      }
    };
    await emailService.sendMail(mailObj);
    return {
      flag: false,
      data: 'Password reset successfully',
    };
  } catch (error) {
    throw new Error(error.message);
  }
};

/**
 * @description : service to send otp via MAIL.
 * @param {string} email : user email required to send mail document.
 * @return {boolean}  : returns status whether MAIL is sent or not.
 */
const sendOTPByEmail = async (email, type) => {
  try {
    let code = common.randomNumber();
    await client.HSET(`${email}_${type}`, {
      code: `${code}`,
      validateLimit: parseInt(MAX_OTP_VALIDATE_RETRY_LIMIT)
    }, {
      EX: REDIS_EXP_TIME
    });
    let mailObj = {
      subject: 'Demo OTP',
      to: email,
      template: '/views/email/OTP',
      data: { code: code, }
    };
    await emailService.sendMail(mailObj);
    return {
      flag: false,
      message: 'Please check your email for OTP',
      data: null
    };
  } catch (error) {
    console.error(error);
    throw new Error(error.message);
  }

};

/**
 * @description : service to verify otp using email.
 * @param {string} email : user email required to send mail document.
 * @return {boolean}  : returns status whether MAIL is sent or not.
 */
const verifyEmailOTP = async (email, code, type) => {
  let isCodeVerify = false;
  let isCodeExpire = false;
  let value = await client.HGETALL(`${email}_${type}`);
  if (_.isEmpty(value) || value.validateLimit <= 0) {
    isCodeExpire = true;
  } else if (value.code === `${code}` && parseInt(value.validateLimit) > 0) {
    await client.del(`${email}_${type}`);
    isCodeVerify = true;
  } else {
    if (value.validateLimit > 0) {
      let newLimit = parseInt(value.validateLimit) - 1
      await client.HSET(`${email}_${type}`, { validateLimit: newLimit });
    }
  }
  if (isCodeVerify) {
    return {
      flag: false,
      data: null,
      message: "Email is verified!"
    };
  } else if (!isCodeVerify && !isCodeExpire) {
    return {
      flag: true,
      data: null,
      message: "OTP Invalid!"
    };
  }
  return {
    flag: true,
    data: null,
    message: "OTP is expired!"
  };
};

const givePermissionForResetPassword = async (email, type) => {
  await client.HSET(`${email}_${type}`, {
    permission: true
  }, {
    EX: REDIS_EXP_TIME
  });
  return true;
};

const verifyResetPasswordPermission = async (email, type) => {
  let value = await client.HGETALL(`${email}_${type}`);
  if (value && value.permission) {
    await client.del(`${email}_${type}`);
    return {
      flag: false,
      data: null,
      message: "You have permission to reset password!"
    };
  }
  return {
    flag: true,
    data: null,
    message: "You don't have permission or your permission expire for reset password!"
  };
};

/**
 * @description : service to send otp via MAIL.
 * @param {string} email : user email required to send mail document.
 * @return {boolean}  : returns status whether MAIL is sent or not.
 */
const resendOTPByEmail = async (obj) => {
  try {
    let updateData;
    let userAuth = await dbService.findOne(model.userAuthSettings, { userId: obj.userId });
    if (userAuth && moment(userAuth[obj.reactiveTime] ? userAuth[obj.reactiveTime] : 0) < moment() && userAuth[obj.retryLimit] < MAX_RESEND_OTP_LIMIT) {
      updateData = { [obj.retryLimit]: userAuth[obj.retryLimit] + 1 };
      if (userAuth[obj.retryLimit] == MAX_RESEND_OTP_LIMIT - 1) {
        updateData = {
          [obj.reactiveTime]: moment().add(RESEND_OTP_REACTIVE_TIME, 'minutes').toISOString(),
          [obj.retryLimit]: 0
        };
      }
      await dbService.update(model.userAuthSettings, { userId: obj.userId }, updateData);
      await sendOTPByEmail(obj.email, obj.type)
      return {
        flag: false,
        message: `OTP sent successfully!.`,
        data: null
      };
    } else {
      console.log(moment().toISOString())
      console.log(moment(userAuth[obj.reactiveTime]).toISOString())
      return {
        flag: true,
        message: `You have exceeded the limit for requesting OTP codes. Please try again in  ${common.getDifferenceOfTwoDatesInTime(moment(), moment(userAuth[obj.reactiveTime]))}.`,
        data: null
      };
    }
  } catch (error) {
    console.error(error);
    throw new Error(error.message);
  }
};

/**
 * @description : service method to send OTP for login.
 * @param {string} username : username of user.
 * @return {obj}  : returns status whether OTP is sent or not. {flag, data}
 */
const socialLoginUser = async (email, flag) => {
  try {
    let user;
    let userCredentials = await dbService.findOne(model.userCredentials, { email: email });
    if (userCredentials) {
      user = await dbService.findOne(model.user, { id: userCredentials.userId });
      if (!user.isSocialLogin) {
        return {
          flag: true,
          message: `you have already account with this email try with password.`,
          data: null
        };
      }
    } else {
      let dataToRegister = {
        email,
        isEmailVerified: true,
        isSocialLogin: true,
        socialLoginType: flag.google ? SOCIAL_LOGIN_TYPE.GOOGLE : SOCIAL_LOGIN_TYPE.FACEBOOK
      }
      user = await dbService.createOne(model.user, dataToRegister);
      userCredentials = await dbService.createOne(model.userCredentials, {
        userId: user.id,
        email,
        name: email.replace(/[^a-zA-Z ]/g, ""),
        userRole: USER_TYPES.User
      });
    }
    let userAuth = await dbService.findOne(model.userAuthSettings, { userId: user.id });
    if (userAuth && userAuth.loginRetryLimit >= MAX_LOGIN_RETRY_LIMIT) {
      let userLimitExceed = await checkLimitOfLoginReactiveTime(userAuth, user);
      if (userLimitExceed.flag) {
        return userLimitExceed;
      }
    }
    if (flag.sendOTP) {
      return await sendOTPByEmail(email, "LOGIN");
    }
    const userData = user.toJSON();
    let token;
    if (!userCredentials.userRole) {
      return {
        flag: true,
        message: 'You have not been assigned any role',
        data: null
      };
    }
    if (!LOGIN_ACCESS[userCredentials.userRole].includes(PLATFORM.CLIENT)) {
      return {
        flag: true,
        message: 'you are not allowed to access this platform',
        data: null
      };
    }
    token = tokenService.generateToken(userData.id, JWT.CLIENT_TOKEN_SECRET);
    let kyc = {}, transData = {};
    if (!user.isKycVerified) {
      kyc = await updateApplicantForKyc(user, 'verify');
    }
    if (user.isCustodialWalletExist && !user.isFirstMaticTransferHappen) {
      transData = await updateFirstTransactionStatus(user.id)
      if (!transData.isFirstMaticTransferHappen) {
        let lastTransaction = await dbService.findAll(model.transaction, { userId: user.id, type: 'DEPOSIT', }, { select: ['status'], sort: { id: -1 } })
        if (lastTransaction.length) {
          transData.lastTransactionStatus = lastTransaction[0].status
        } else {
          transData.lastTransactionStatus = null
        }
      }
    }
    if (userAuth && userAuth.loginRetryLimit) {
      await dbService.update(model.userAuthSettings, { userId: user.id }, {
        loginRetryLimit: 0,
        loginReactiveTime: null
      });
    }
    let userToReturn = {
      ...userData,
      ...{ email },
      ...kyc.data,
      ...transData,
      token
    };
    return {
      flag: false,
      data: userToReturn
    };
  } catch (error) {
    console.error(error)
    throw new Error(error.message);
  }
};

const sendOTPForSocialLogin = async (email, flag) => {
  try {
    return await socialLoginUser(email, { ...flag, ...{ sendOTP: true } });
  } catch (error) {
    throw new Error(error.message);
  }
};

/**
 * @description : service to login with OTP.
 * @param {string} username : username of user.
 * @param {string} password : password of user.
 * @param {string} platform : platform.
 * @return {obj}  : returns authentication object {flag,status}.
 */
const socialLoginWithOTP = async (email, flag) => {
  try {
    return await socialLoginUser(email, flag);
  } catch (error) {
    throw new Error(error.message);
  }
};

module.exports = {
  loginUser,
  sendLoginOTP,
  loginWithOTP,
  resetPassword,
  sendOTPByEmail,
  verifyEmailOTP,
  givePermissionForResetPassword,
  verifyResetPasswordPermission,
  resendOTPByEmail,
  sendOTPForSocialLogin,
  socialLoginWithOTP,
  checkUserWithEmailVerifiedExist
}; 