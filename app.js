/**
 * app.js
 * Use `app.js` to run your app.
 * To start the server, run: `node app.js`.
 */

const express = require('express');
const cors = require('cors');
const path = require('path');
const dotenv = require('dotenv');
dotenv.config({ path: '.env' });
global.__basedir = __dirname;
const passport = require('passport');
const multer = require('multer');
const store = multer.diskStorage({ destination: 'uploads/' });
const upload = multer({
  storage: store,
  limits: {
    fileSize: 8000000 // Compliant: 8MB
  }
});
//all routes 
const routes = require('./routes');
let logger = require('morgan');
const { clientSession } = require('./middleware/redisClient');

const { clientPassportStrategy } = require('./config/clientPassportStrategy');
const { adminPassportStrategy } = require('./config/adminPassportStrategy');
const bodyParser = require('body-parser');

const app = express();
app.use(require('./utils/response/responseHandler'));
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger/swagger_output.json')

app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerFile))

const httpServer = require('http').createServer(app);

app.use(clientSession);
const corsOptions = { origin: process.env.ALLOW_ORIGIN, };
app.use(cors(corsOptions));

//template engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

clientPassportStrategy(passport);
adminPassportStrategy(passport);

app.use(bodyParser.json({
  verify: (req, res, buf) => {
    req.rawBody = buf
  }
}))

app.use(logger('dev'));
app.use(upload.array('files'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'uploads')));
app.use(routes);

app.get('/', (req, res) => {
  res.status(200).send('Successfully Connected');
});

if (process.env.NODE_ENV !== 'test') {
  httpServer.listen(process.env.PORT, () => {
    console.log(`your application is running on ${process.env.PORT}`);
  });
} else {
  module.exports = app;
}
