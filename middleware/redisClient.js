const { createClient } = require('redis');
const session = require("express-session");
let RedisStore = require("connect-redis")(session);
const { SESSION_MAX_AGE, SESSION_NAME, SESSION_SECRET } = require('../constants/authConstant');

const options = {
    port: process.env.REDIS_PORT, // replace with your port
    host: process.env.REDIS_HOST, // replace with your hostname or IP address
    password: process.env.REDIS_PASSWORD, // replace with your hostname or IP address
};

const client = createClient({ url: `redis://${options.host}:${options.port}` });

client.connect();

client.on('connect', () => {
    console.info("Client connected to redis", options.host, options.port);
})

client.on('ready', () => {
    console.log("Client connected to redis ready to use...");
})

client.on('error', (err) => {
    console.log(err.message);
})

client.on('end', () => {
    console.log("Client disconnected from redis");
})

client.on('SIGINT', () => {
    client.quit();
});

let clientSession = session({
    name: SESSION_NAME,
    store: new RedisStore({
        client: client
    }),
    saveUninitialized: false,
    resave: false,
    secret: SESSION_SECRET,
    cookie: {
        maxAge: SESSION_MAX_AGE,
        sameSite: true,
        secure: true
    }
});


module.exports = {
    client,
    clientSession
}

