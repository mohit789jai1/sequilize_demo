/**
 * userConstant.js
 * @description :: constants used in user module
 */
  
  const userAttribute = ["name","mobileNo","kycId","applicantKycId","custodialWalletAddress"]

  const PROJECT_SETTING = {
    CURRENCY_TOKEN_VALUE: 1
  }
  
  module.exports = {
    userAttribute,
    PROJECT_SETTING
  };