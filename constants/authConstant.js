/**
 * authConstant.js
 * @description :: constants used in authentication
 */

const JWT = {
  CLIENT_TOKEN_SECRET: "ajsfbckhacjbcsdhsdkvbshvhbshbcskdcbskjvsolvikcsdcjsbdviksvjdsoidnsnscsmc",
  ACCESS_TOKEN_EXPIRES_IN: "5m",
  REFRESH_TOKEN_EXPIRES_IN: "15m",
  ADMIN_TOKEN_SECRET: "ufjsdbhsniudhjconujfgbuidrdyeihkvjdbndfjgfbeijdekqpffgyherbjefnvbeqrhgbhr"
};

const USER_TYPES = {
  User: 1,
  SuperAdmin: 10,
  Admin: 11
};

const PLATFORM = {
  CLIENT: 1,
  ADMIN: 2
};

let LOGIN_ACCESS = {
  [USER_TYPES.User]: [PLATFORM.CLIENT],
  [USER_TYPES.SuperAdmin]: [PLATFORM.ADMIN],
  [USER_TYPES.Admin]: [PLATFORM.ADMIN],
};

const MAX_LOGIN_RETRY_LIMIT = 100;
const MAX_OTP_VALIDATE_RETRY_LIMIT = 100;
const LOGIN_REACTIVE_TIME = 0;
const MAX_RESEND_OTP_LIMIT = 100;
const RESEND_OTP_REACTIVE_TIME = 0;
const REDIS_EXP_TIME = 3600; // 10 min or 600 sec
const SESSION_MAX_AGE = 1000 * 60 * 60 * 2;
const SESSION_SECRET = "wqiuyeuiywpoghsgdhjanxcbnbvbmvrfnvirn979798eszdrxftgybnhjumji";
const SESSION_NAME = "USER INFO";

const FORGOT_PASSWORD_LIMITS = {
  LINK: {
    email: true,
    sms: false
  },
  EXPIRE_TIME: 10
};

const SOCIAL_LOGIN_TYPE = {
  GOOGLE: 1,
  FACEBOOK: 2
};

const SOCIAL_LOGIN_CLIENT_DETAILS = {
  GOOGLE_CLIENT_ID: "",
  GOOGLE_CLIENT_SECRET: "",
  FACEBOOK_CLIENT_ID: "your-client-id",
  FACEBOOK_CLIENT_SECRET: "your-client-secret"
};

const KYC_STATUS = {
  default1: 0,
  default2: null,
  init: 1,
  pending: 2,
  prechecked: 3,
  queued: 4,
  completed: 5,
  onHold: 6
}

module.exports = {
  JWT,
  USER_TYPES,
  PLATFORM,
  MAX_LOGIN_RETRY_LIMIT,
  LOGIN_REACTIVE_TIME,
  MAX_RESEND_OTP_LIMIT,
  RESEND_OTP_REACTIVE_TIME,
  REDIS_EXP_TIME,
  SESSION_MAX_AGE,
  SESSION_SECRET,
  SESSION_NAME,
  FORGOT_PASSWORD_LIMITS,
  MAX_OTP_VALIDATE_RETRY_LIMIT,
  LOGIN_ACCESS,
  SOCIAL_LOGIN_TYPE,
  SOCIAL_LOGIN_CLIENT_DETAILS,
  KYC_STATUS
};