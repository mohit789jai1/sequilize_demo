/**
 * transaction.js
 * @description :: sequelize model of database table transaction
 */

const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { reusableModelAttribute } = require('../utils/common');
let UserTransactions = sequelize.define('transaction',
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        userId: {
            type: DataTypes.INTEGER,
        },
        fromAddress: {
            type: DataTypes.STRING
        },
        toAddress: {
            type: DataTypes.STRING
        },
        fromValue: {
            type: DataTypes.STRING
        },
        toValue: {
            type: DataTypes.STRING
        },
        fromTokenSymbol: {
            type: DataTypes.STRING
        },
        toTokenSymbol: {
            type: DataTypes.STRING
        },
        transactionHash: {
            type: DataTypes.STRING
        },
        gasFees: {
            type: DataTypes.STRING
        },
        gasSymbol: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.STRING
        },
        type: {
            type: DataTypes.STRING
        },
        requestId: {
            type: DataTypes.STRING
        },
        valueInUSD: {
            type: DataTypes.STRING
        },
        timestamps: {
            type: DataTypes.INTEGER
        },
        storeUpdate: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
    }
    , {
        freezeTableName: true,
        hooks: {
            beforeCreate: [],
            beforeBulkCreate: [],
        }
    }
);
UserTransactions.prototype.toJSON = function () {
    return Object.assign({}, this.get());
};
sequelizeTransforms(UserTransactions);
sequelizePaginate.paginate(UserTransactions);
module.exports = UserTransactions;
