/**
 * propertySetting.js
 * @description :: sequelize model of database table Property Setting
 */

const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { convertObjectToEnum, reusableModelAttribute } = require('../utils/common');
const userConstantEnum = require('../constants/userConstant');
let projectSetting = sequelize.define('projectSetting',
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        type: {
            type: DataTypes.INTEGER,
            required: true,
            values: convertObjectToEnum(userConstantEnum.PROJECT_SETTING)
        },
        setting: {
            type: DataTypes.JSON,
        },
        ...reusableModelAttribute,
    }, {
    freezeTableName: true,
    hooks: {
        beforeCreate: [
            async function (setting, options) {
                setting.isActive = true;
                setting.isDeleted = false;
            },
        ],
    }
}
);
projectSetting.prototype.toJSON = function () {
    return Object.assign({}, this.get());
};
sequelizeTransforms(projectSetting);
sequelizePaginate.paginate(projectSetting);
module.exports = projectSetting;
