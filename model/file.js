/**
 * file.js
 * @description :: sequelize model of database table file
 */

const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { reusableModelAttribute } = require('../utils/common');
let File = sequelize.define('file',
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
        },
        uri: {
            type: DataTypes.STRING,
        },
        width: {
            type: DataTypes.INTEGER,
        },
        height: {
            type: DataTypes.INTEGER,
        },
        size: {
            type: DataTypes.INTEGER,
        },
        mimeType: {
            type: DataTypes.STRING,
        },
        slug: {
            type: DataTypes.STRING,
        },
        type: {
            type: DataTypes.STRING,
        },
        link: {
            type: DataTypes.STRING,
        },
        title: {
            type: DataTypes.STRING,
        },
        description: {
            type: DataTypes.STRING,
        },
        ...reusableModelAttribute
    }, {
    freezeTableName: true,
    hooks: {
        beforeCreate: [
            async function (file, options) {
                file.isActive = true;
                file.isDeleted = false;
                file.addedBy = file.userId;
                file.updatedBy = file.userId;
            },
        ],
        beforeBulkCreate: [
            async function (file, options) {
                if (file !== undefined && file.length) {
                    for (let fileIndex of file) {
                        const element = fileIndex;
                        element.isActive = true;
                        element.isDeleted = false;
                    }
                }
            },
        ],
    }
}
);
File.prototype.toJSON = function () {
    return Object.assign({}, this.get());
};
sequelizeTransforms(File);
sequelizePaginate.paginate(File);
module.exports = File;