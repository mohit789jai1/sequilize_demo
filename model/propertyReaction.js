/**
 * Property.js
 * @description :: sequelize model of database table Property
 */

const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { convertObjectToEnum, reusableModelAttribute } = require('../utils/common');
const propertyConstantEnum = require('../constants/propertyConstant');
let propertyReaction = sequelize.define('propertyReaction',
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        propertyId: {
            type: DataTypes.INTEGER,
        },
        userId: {
            type: DataTypes.INTEGER,
        },
        reactionType: {
            type: DataTypes.INTEGER,
            required: true,
            values: convertObjectToEnum(propertyConstantEnum.REACTION_TYPE)
        },
        ...reusableModelAttribute,
    }, {
    freezeTableName: true,
    hooks: {
        beforeCreate: [
            async function (reaction, options) {
                reaction.isActive = true;
                reaction.isDeleted = false;
                reaction.addedBy = reaction.userId;
                reaction.updatedBy = reaction.userId;
            },
        ],
    }
}
);
propertyReaction.prototype.toJSON = function () {
    return Object.assign({}, this.get());
};
sequelizeTransforms(propertyReaction);
sequelizePaginate.paginate(propertyReaction);
module.exports = propertyReaction;
