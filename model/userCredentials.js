/**
 * user.js
 * @description :: sequelize model of database table user
 */

const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { convertObjectToEnum, reusableModelAttribute } = require('../utils/common');
const bcrypt = require('bcrypt');
const authConstantEnum = require('../constants/authConstant');
let userCredentials = sequelize.define('userCredentials', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  userId: {
    type: DataTypes.INTEGER,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false
  },
  password: {
    type: DataTypes.STRING,
    allowNull: true
  },
  userRole: {
    type: DataTypes.INTEGER,
    required: true,
    values: convertObjectToEnum(authConstantEnum.USER_TYPES)
  },
  ...reusableModelAttribute,
}
  , {
    freezeTableName: true,
    hooks: {
      beforeCreate: [
        async function (userCredentials, options) {
          if (userCredentials.password) {
            userCredentials.password =
              await bcrypt.hash(userCredentials.password, 8);
          }
          userCredentials.isActive = true;
          userCredentials.isDeleted = false;
          userCredentials.addedBy = userCredentials.userId;
          userCredentials.updatedBy = userCredentials.userId;
        },
      ],
      beforeBulkCreate: [
        async function (userCredentials, options) {
          if (userCredentials !== undefined && userCredentials.length) {
            for (let userCredentialsIndex of userCredentials) {
              const element = userCredentialsIndex;
              if (element.password) {
                element.password = await bcrypt.hash(element.password, 8);
              }
              element.isActive = true;
              element.isDeleted = false;
            }
          }
        },
      ],
      afterCreate: [
        async function (userCredentials, options) {
          sequelize.model('userAuthSettings').create({ userId: userCredentials.userId });
        },
      ],
    }
  }
);
userCredentials.prototype.isPasswordMatch = async function (password) {
  const userCredentials = this;
  return bcrypt.compare(password, userCredentials.password);
};
userCredentials.prototype.toJSON = function () {
  let values = Object.assign({}, this.get());
  delete values.password;
  return values;
};
sequelizeTransforms(userCredentials);
sequelizePaginate.paginate(userCredentials);
module.exports = userCredentials;
