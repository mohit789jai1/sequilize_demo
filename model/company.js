/**
 * Company.js
 * @description :: sequelize model of database table company name
 */

 const { DataTypes } = require('sequelize');
 const sequelize = require('../config/dbConnection');
 const sequelizePaginate = require('sequelize-paginate');
 const sequelizeTransforms = require('sequelize-transforms');
 const { reusableModelAttribute } = require('../utils/common');
 let company = sequelize.define('company',
     {
         id: {
             type: DataTypes.INTEGER,
             autoIncrement: true,
             primaryKey: true
         },
         companyName: {
             type: DataTypes.STRING
         },
         ...reusableModelAttribute,
     }, {
     freezeTableName: true,
     hooks: {
         beforeCreate: [
             async function (Company, options) {
                Company.isActive = false;
                Company.isDeleted = false;
                Company.addedBy = company.userId;
                Company.updatedBy = company.userId;
             },
         ],
         beforeBulkCreate: [
             async function (Company, options) {
                 if (Company !== undefined && Company.length) {
                     for (let companyIndex of Company) {
                         const element = companyIndex;
                         element.isActive = false;
                         element.isDeleted = false;
                     }
                 }
             },
         ],
     }
 }
 );
 company.prototype.toJSON = function () {
     return Object.assign({}, this.get());
 };
 sequelizeTransforms(company);
 sequelizePaginate.paginate(company);
 module.exports = company;
 