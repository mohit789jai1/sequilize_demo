/**
 * userAuthSettings.js
 * @description :: sequelize model of database table userAuthSettings
 */

const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { reusableModelAttribute } = require('../utils/common');
let UserAuthSettings = sequelize.define('userAuthSettings',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    userId: {
      type: DataTypes.INTEGER
    },
    loginRetryLimit: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    loginReactiveTime: {
      type: DataTypes.DATE
    },
    loginResendOTPRetryLimit: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    loginResendOTPReactiveTime: {
      type: DataTypes.DATE
    },
    verifyEmailResendOTPRetryLimit: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    verifyEmailResendOTPReactiveTime: {
      type: DataTypes.DATE
    },
    altEmail: {
      type: DataTypes.STRING
    },
    resetPasswordResendOTPRetryLimit: {
      type: DataTypes.INTEGER
    },
    resetPasswordResendOTPReactiveTime: {
      type: DataTypes.DATE
    },
    ...reusableModelAttribute,
  }
  , {
    freezeTableName: true,
    hooks: {
      beforeCreate: [
        async function (userAuthSettings, options) {
          userAuthSettings.isActive = true;
          userAuthSettings.isDeleted = false;
          userAuthSettings.addedBy = userAuthSettings.userId;
          userAuthSettings.updatedBy = userAuthSettings.userId;
        },
      ],
      beforeBulkCreate: [
        async function (userAuthSettings, options) {
          if (userAuthSettings !== undefined && userAuthSettings.length) {
            for (let userAuthSettingsIndex of userAuthSettings) {
              const element = userAuthSettingsIndex;
              element.isActive = true;
              element.isDeleted = false;
            }
          }
        },
      ],
    }
  }
);
UserAuthSettings.prototype.toJSON = function () {
  return Object.assign({}, this.get());
};
sequelizeTransforms(UserAuthSettings);
sequelizePaginate.paginate(UserAuthSettings);
module.exports = UserAuthSettings;
