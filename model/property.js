/**
 * Property.js
 * @description :: sequelize model of database table Property
 */

const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { convertObjectToEnum, reusableModelAttribute } = require('../utils/common');
const propertyConstantEnum = require('../constants/propertyConstant');
let Property = sequelize.define('property',
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        userId: {
            type: DataTypes.INTEGER,
        },
        companyName: {
            type: DataTypes.STRING,
        },
        contactName: {
            type: DataTypes.STRING,
        },
        contactEmail: {
            type: DataTypes.STRING,
        },
        contactPhone: {
            type: DataTypes.STRING,
        },
        addressLine1: {
            type: DataTypes.STRING,
        },
        addressLine2: {
            type: DataTypes.STRING,
        },
        country: {
            type: DataTypes.STRING,
        },
        city: {
            type: DataTypes.STRING,
        },
        postCode: {
            type: DataTypes.STRING,
        },
        propertyOwnership: {
            type: DataTypes.INTEGER,
            required: true,
            values: convertObjectToEnum(propertyConstantEnum.PROPERTY_OWNER_TYPES)
        },
        propertyProfileImages: {
            type: DataTypes.ARRAY(DataTypes.INTEGER),
        },
        currencySymbol: {
            type: DataTypes.STRING,
        },
        assetValue: {
            type: DataTypes.BIGINT,
            defaultValue: 0
        },
        isOutdoorMedia: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        assetClass: {
            type: DataTypes.STRING,
        },
        location: {
            type: DataTypes.STRING,
        },
        marketSize: {
            type: DataTypes.STRING,
        },
        digitalRightsValue: {
            type: DataTypes.DOUBLE,
            defaultValue: 0
        },
        isFreeze: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        propertyName: {
            type: DataTypes.STRING,
        },
        propertyDescription: {
            type: DataTypes.TEXT,
        },
        propertyStatus: {
            type: DataTypes.INTEGER,
            required: true,
            values: convertObjectToEnum(propertyConstantEnum.PROPERTY_STATUS)
        },
        ...reusableModelAttribute,
    }, {
    freezeTableName: true,
    hooks: {
        beforeCreate: [
            async function (property, options) {
                property.isActive = true;
                property.isDeleted = false;
                property.addedBy = property.userId;
                property.updatedBy = property.userId;
            },
        ],
        beforeBulkCreate: [
            async function (property, options) {
                if (property !== undefined && property.length) {
                    for(let propertyIndex of property) {
                      const element = propertyIndex;
                      element.isActive = true;
                      element.isDeleted = false;
                    }
                  }
            },
        ],
    }
}
);
Property.prototype.toJSON = function () {
    return Object.assign({}, this.get());
};
sequelizeTransforms(Property);
sequelizePaginate.paginate(Property);
module.exports = Property;
