/**
 * user.js
 * @description :: sequelize model of database table user
 */

const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { convertObjectToEnum, reusableModelAttribute } = require('../utils/common');
const bcrypt = require('bcrypt');
const authConstantEnum = require('../constants/authConstant');
let User = sequelize.define('user', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  isEmailVerified: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  name: {
    type: DataTypes.STRING
  },
  companyId: {
    type: DataTypes.INTEGER,
  },
  mobileNo: {
    type: DataTypes.STRING,
    allowNull: true
  },
  kycId: {
    type: DataTypes.STRING,
    allowNull: true
  },
  applicantKycId: {
    type: DataTypes.STRING,
    allowNull: true
  },
  kycStatus: {
    type: DataTypes.INTEGER,
    values: convertObjectToEnum(authConstantEnum.KYC_STATUS)
  },
  isKycVerified: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  vaultId: {
    type: DataTypes.STRING,
    allowNull: true
  },
  custodialWalletAddress: {
    type: DataTypes.STRING,
    allowNull: true
  },
  isCustodialWalletExist: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  isSocialLogin: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  socialLoginType: {
    type: DataTypes.INTEGER,
    allowNull: true
  },
  isFirstMaticTransferHappen: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },

  ...reusableModelAttribute
}
  , {
    freezeTableName: true,
    hooks: {
      beforeCreate: [
        async function (user, options) {
          user.isActive = true;
          user.isDeleted = false;

        },
      ],
      beforeBulkCreate: [
        async function (user, options) {
          if (user !== undefined && user.length) {
            for (let userIndex of user) {
              const element = userIndex;
              element.isActive = true;
              element.isDeleted = false;
            }
          }
        },
      ],
      afterCreate: [
        async function (user, options) {
          sequelize.model('user').update({ addedBy: user.id, updatedBy: user.id }, { where: { id: user.id } });
        },
      ],
    }
  }
);
User.prototype.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};
User.prototype.toJSON = function () {
  let values = Object.assign({}, this.get());
  delete values.password;
  return values;
};
sequelizeTransforms(User);
sequelizePaginate.paginate(User);
module.exports = User;
