/**
 * index.js
 * @description :: exports all the models and its relationships among other models
 */

const dbConnection = require('../config/dbConnection');
dbConnection.sync({});
let db = {};
db.sequelize = dbConnection;
db.user = require('./user');
db.userAuthSettings = require('./userAuthSettings');
db.userCredentials = require('./userCredentials');
db.transaction = require('./transaction');
db.property = require('./property');
db.file = require('./file');
db.propertyReaction = require('./propertyReaction');
db.projectSetting = require('./projectSetting');
db.company = require('./company');
//user
db.user.belongsTo(db.user, {
  foreignKey: 'addedBy',
  as: '_addedBy',
  targetKey: 'id'
});
db.user.hasMany(db.user, {
  foreignKey: 'addedBy',
  sourceKey: 'id'
});
db.user.belongsTo(db.user, {
  foreignKey: 'updatedBy',
  as: '_updatedBy',
  targetKey: 'id'
});
db.user.hasMany(db.user, {
  foreignKey: 'updatedBy',
  sourceKey: 'id'
});

db.user.hasMany(db.property, {
  foreignKey: 'userId',
  sourceKey: 'id'
});
db.user.hasMany(db.company, {
  foreignKey: 'id',
  sourceKey: 'companyId'
});

//userAuthSettings
db.userAuthSettings.belongsTo(db.user, {
  foreignKey: 'userId',
  as: '_userId',
  targetKey: 'id'
});
db.user.hasMany(db.userAuthSettings, {
  foreignKey: 'userId',
  sourceKey: 'id'
});
db.userAuthSettings.belongsTo(db.user, {
  foreignKey: 'addedBy',
  as: '_addedBy',
  targetKey: 'id'
});
db.user.hasMany(db.userAuthSettings, {
  foreignKey: 'addedBy',
  sourceKey: 'id'
});
db.userAuthSettings.belongsTo(db.user, {
  foreignKey: 'updatedBy',
  as: '_updatedBy',
  targetKey: 'id'
});
db.user.hasMany(db.userAuthSettings, {
  foreignKey: 'updatedBy',
  sourceKey: 'id'
});

//userCredentials
db.userCredentials.belongsTo(db.user, {
  foreignKey: 'userId',
  as: '_userId',
  targetKey: 'id'
});
db.user.hasOne(db.userCredentials, {
  foreignKey: 'userId',
  sourceKey: 'id'
});
db.userCredentials.belongsTo(db.user, {
  foreignKey: 'addedBy',
  as: '_addedBy',
  targetKey: 'id'
});
db.user.hasMany(db.userCredentials, {
  foreignKey: 'addedBy',
  sourceKey: 'id'
});
db.userCredentials.belongsTo(db.user, {
  foreignKey: 'updatedBy',
  as: '_updatedBy',
  targetKey: 'id'
});
db.user.hasMany(db.userCredentials, {
  foreignKey: 'updatedBy',
  sourceKey: 'id'
});

//transaction
db.transaction.belongsTo(db.user, {
  foreignKey: 'userId',
  as: '_userId',
  targetKey: 'id'
});
db.user.hasMany(db.transaction, {
  foreignKey: 'userId',
  sourceKey: 'id'
});

//properties
db.property.belongsTo(db.user, {
  foreignKey: 'userId',
  targetKey: 'id'
});

db.property.belongsTo(db.userCredentials, {
  foreignKey: 'userId',
});

db.property.hasMany(db.propertyReaction, {
  foreignKey: 'propertyId',
  sourceKey: 'id'
});

//properties reaction
db.propertyReaction.belongsTo(db.property, {
  foreignKey: 'propertyId'
});

module.exports = db;